﻿using Education.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Education.Master;
using System.Web;


namespace SIND.Service
{
   public  class ErrorTrackService
    {
       public void InserErrorLog(string StackTrace,string Message)
       {
           try
           {
               string ModuleName = HttpContext.Current.Request.RequestContext.RouteData.Values["controller"].ToString();
               string Page = HttpContext.Current.Request.Url.AbsoluteUri;
               using (var repo = new EfUnitOfWork())
               {
                   int Module = repo.ModulesRepository.FindMany(s => s.ModuleName == ModuleName).Select(s => s.Id).FirstOrDefault();
                   var ErrorTable = new ErrorLog
                   {
                       ModulesId = Module,
                       Stacktrace = StackTrace,
                       Message = Message,
                       Page = Page
                   };
                   repo.ErrorLogRepository.InsertOrUpdate(ErrorTable);
                   repo.Commit();
               }
           }
           catch (Exception ex)
           {
               //Code for exception handling
           }
         

       }
    }
}
