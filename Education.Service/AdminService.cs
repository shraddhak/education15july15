﻿using Education.DataAccess;
using Education.Dtos;
using Education.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Service
{
    public class AdminService
    {        
        //Branch
        public void AddBranch(string branchName)
        {
            using (var repo = new EfUnitOfWork())
            {
                var branch = new Branch() { BrachTitle = branchName };
                repo.BranchRepository.InsertOrUpdate(branch);
                repo.Commit();
                repo.Dispose();
            }
        }

        public BranchDto GetBranchById(int branchId)
        {
            using (var repo = new EfUnitOfWork())
            {
                var data = repo.BranchRepository.FindMany(s => s.Id == branchId).FirstOrDefault();
                return new BranchDto
                {
                    Id = data.Id,
                    BrachTitle = data.BrachTitle
                };
            }
        }

        public void UpdateBranch(BranchDto branch)
        {
            using (var repo = new EfUnitOfWork())
            {
                repo.BranchRepository.InsertOrUpdate(new Branch { Id = branch.Id, BrachTitle = branch.BrachTitle });
                repo.Commit();
                repo.Dispose();
            }
        }

        public void DeleteBranch(int branchId)
        {
            using (var repo = new EfUnitOfWork())
            {
                repo.BranchRepository.Delete(branchId);
                repo.Commit();
                repo.Dispose();
            }
        }

        //Year
        public void AddYear(YearDto yearDto)
        {
            using (var repo = new EfUnitOfWork())
            {
                var year = new Year() { YearTitle = yearDto.YearTitle, BranchId = yearDto.BranchId };
                repo.YearRepository.InsertOrUpdate(year);
                repo.Commit();
                repo.Dispose();
            }
        }

        public YearDto GetYearById(int yearId)
        {            
            using (var repo = new EfUnitOfWork())
            {
                var data = repo.YearRepository.FindMany(s => s.Id == yearId).FirstOrDefault();
                return new YearDto
                {
                    Id = data.Id,
                    YearTitle = data.YearTitle,
                    BranchId = data.BranchId
                };
            }            
        }

        public void UpdateYear(YearDto year)
        {
            using (var repo = new EfUnitOfWork())
            {
                repo.YearRepository.InsertOrUpdate(new Year { Id = year.Id, YearTitle = year.YearTitle, BranchId = year.BranchId });
                repo.Commit();
                repo.Dispose();
            }
        }

        public List<YearDto> GetYear()
        {
            using (var repo = new EfUnitOfWork())
            {
                var lst = new List<YearDto>();
                var data = repo.YearRepository.FindMany(s => s.YearTitle != null).ToList();
                foreach (var item in data)
                {
                    lst.Add(new YearDto { Id = item.Id, YearTitle = item.YearTitle, BranchId = item.BranchId });
                }
                repo.Commit();
                repo.Dispose();
                return lst;
            }
        }

        public void DeleteYear(int yearId)
        {
            using (var repo = new EfUnitOfWork())
            {
                repo.YearRepository.Delete(yearId);
                repo.Commit();
                repo.Dispose();
            }
        }

        //Semester
        public void AddSemester(SemesterDto SemesterDto)
        {
            using (var repo = new EfUnitOfWork())
            {
                var semester = new Semester() { SemesterTitle = SemesterDto.SemesterTitle, YearId = SemesterDto.YearId };
                repo.SemesterRepository.InsertOrUpdate(semester);
                repo.Commit();
                repo.Dispose();
            }
        }

        public SemesterDto GetSemesterById(int SemesterId)
        {
            using (var repo = new EfUnitOfWork())
            {
                var data = repo.SemesterRepository.FindMany(s => s.Id == SemesterId).FirstOrDefault();
                return new SemesterDto
                {
                    Id = data.Id,
                    SemesterTitle = data.SemesterTitle,
                    YearId = data.YearId
                };
            }
        }

        public void UpdateSemester(SemesterDto Semester)
        {
            using (var repo = new EfUnitOfWork())
            {
                repo.SemesterRepository.InsertOrUpdate(new Semester { Id = Semester.Id, SemesterTitle = Semester.SemesterTitle, YearId = Semester.YearId });
                repo.Commit();
                repo.Dispose();
            }
        }

        public List<SemesterDto> GetSemester()
        {
            using (var repo = new EfUnitOfWork())
            {
                var lst = new List<SemesterDto>();
                var data = repo.SemesterRepository.FindMany(s => s.SemesterTitle != null).ToList();
                foreach (var item in data)
                {
                    lst.Add(new SemesterDto { Id = item.Id, SemesterTitle = item.SemesterTitle, YearId = item.YearId });
                }
                repo.Commit();
                repo.Dispose();
                return lst;
            }
        }

        public void DeleteSemester(int semId)
        {
            using (var repo = new EfUnitOfWork())
            {
                repo.SemesterRepository.Delete(semId);
                repo.Commit();
                repo.Dispose();
            }
        }

        //Subjects
        public List<SubjectDto> GetSubject()
        {
            using (var repo = new EfUnitOfWork())
            {
                var lst = new List<SubjectDto>();
                var data = repo.SubjectRepository.FindMany(s => s.SubjectTitle != null).ToList();
                foreach (var item in data)
                {
                    lst.Add(new SubjectDto { Id = item.Id, SubjectTitle = item.SubjectTitle, SemesterId = item.SemesterId });
                }
                repo.Commit();
                repo.Dispose();
                return lst;
            }
        }

        public void AddSubject(SubjectDto subjectDto)
        {
            using (var repo = new EfUnitOfWork())
            {
                var semester = new Subject() { SubjectTitle = subjectDto.SubjectTitle, SemesterId = subjectDto.SemesterId };
                repo.SubjectRepository.InsertOrUpdate(semester);
                repo.Commit();
                repo.Dispose();
            }
        }

        public SubjectDto GetSubjectById(int subjectId)
        {
            using (var repo = new EfUnitOfWork())
            {
                var data = repo.SubjectRepository.FindMany(s => s.Id == subjectId).FirstOrDefault();
                return new SubjectDto
                {
                    Id = data.Id,
                    SubjectTitle = data.SubjectTitle,
                    SemesterId = data.SemesterId
                };
            }
        }

        public void UpdateSubject(SubjectDto subject)
        {
            using (var repo = new EfUnitOfWork())
            {
                repo.SubjectRepository.InsertOrUpdate(new Subject { Id = subject.Id, SubjectTitle = subject.SubjectTitle, SemesterId = subject.SemesterId });
                repo.Commit();
                repo.Dispose();
            }
        }

        public void DeleteSubject(int subjectId)
        {
            using (var repo = new EfUnitOfWork())
            {
                repo.SubjectRepository.Delete(subjectId);
                repo.Commit();
                repo.Dispose();
            }
        }

        //Chapter
        public List<ChapterDto> GetChapter()
        {
            using (var repo = new EfUnitOfWork())
            {
                var lst = new List<ChapterDto>();
                var data = repo.ChapterRepository.FindMany(s => s.Title != null).ToList();
                foreach (var item in data)
                {
                    lst.Add(new ChapterDto { Id = item.Id, Title = item.Title, SubjectId = item.SubjectId });
                }
                repo.Commit();
                repo.Dispose();
                return lst;
            }
        }

        public void AddChapter(ChapterDto chap)
        {
            using (var repo = new EfUnitOfWork())
            {
                var chapter = new Chapter() { Title = chap.Title, SubjectId = chap.SubjectId };
                repo.ChapterRepository.InsertOrUpdate(chapter);
                repo.Commit();
                repo.Dispose();
            }
        }

        public ChapterDto GetChapterById(int chapId)
        {
            using (var repo = new EfUnitOfWork())
            {
                var data = repo.ChapterRepository.FindMany(s => s.Id == chapId).FirstOrDefault();
                return new ChapterDto
                {
                    Id = data.Id,
                    Title = data.Title,
                    SubjectId = data.SubjectId
                };
            }
        }

        public void UpdateChapter(ChapterDto chapter)
        {
            using (var repo = new EfUnitOfWork())
            {
                repo.ChapterRepository.InsertOrUpdate(new Chapter { Id = chapter.Id, Title = chapter.Title, SubjectId = chapter.SubjectId });
                repo.Commit();
                repo.Dispose();
            }
        }

        public void DeleteChapter(int chapId)
        {
            using (var repo = new EfUnitOfWork())
            {
                repo.ChapterRepository.Delete(chapId);
                repo.Commit();
                repo.Dispose();
            }
        }

        //SubscriptionDetails
        public List<SubscriptionDetailDto> GetSubscriptionDetail()
        {
            using (var repo = new EfUnitOfWork())
            {
                var lst = new List<SubscriptionDetailDto>();
                var data = repo.SubdtlRepository.All;
                foreach (var item in data)
                {
                    lst.Add(new SubscriptionDetailDto 
                    { 
                        Id = item.Id, 
                        ContentId = item.ContentId, 
                        UserId = item.UserId, 
                        StartTime = item.StartTime, 
                        EndTime = item.EndTime, 
                        UserSubscriptionId = item.UserSubscriptionId, 
                        SubscriptionTypeId = item.SubscriptionTypeId 
                    });
                }
                repo.Commit();
                repo.Dispose();
                return lst;
            }
        }

        public void AddSubscriptionDetail(SubscriptionDetailDto sub)
        {
            using (var repo = new EfUnitOfWork())
            {
                var subDetail = new SubscriptionDetails() 
                {
                    ContentId = sub.ContentId,
                    UserId = sub.UserId,
                    StartTime = sub.StartTime,
                    EndTime = sub.EndTime,
                    UserSubscriptionId = sub.UserSubscriptionId,
                    SubscriptionTypeId = sub.SubscriptionTypeId  
                };
                repo.SubdtlRepository.InsertOrUpdate(subDetail);
                repo.Commit();
                repo.Dispose();
            }
        }

        public SubscriptionDetailDto GetSubscriptionDetailById(int subDetailId)
        {
            using (var repo = new EfUnitOfWork())
            {
                var item = repo.SubdtlRepository.FindMany(s => s.Id == subDetailId).FirstOrDefault();
                return new SubscriptionDetailDto
                {
                    Id = item.Id,
                    ContentId = item.ContentId,
                    UserId = item.UserId,
                    StartTime = item.StartTime,
                    EndTime = item.EndTime,
                    UserSubscriptionId = item.UserSubscriptionId,
                    SubscriptionTypeId = item.SubscriptionTypeId 
                };
            }
        }

        public void UpdateSubscriptionDetail(SubscriptionDetailDto subDetail)
        {
            using (var repo = new EfUnitOfWork())
            {
                repo.SubdtlRepository.InsertOrUpdate(new SubscriptionDetails 
                {
                    Id = subDetail.Id,
                    ContentId = subDetail.ContentId,
                    UserId = subDetail.UserId,
                    StartTime = subDetail.StartTime,
                    EndTime = subDetail.EndTime,
                    UserSubscriptionId = subDetail.UserSubscriptionId,
                    SubscriptionTypeId = subDetail.SubscriptionTypeId 
                });
                repo.Commit();
                repo.Dispose();
            }
        }

        public void DeleteSubscriptionDetail(int subDetailId)
        {
            using (var repo = new EfUnitOfWork())
            {
                repo.SubdtlRepository.Delete(subDetailId);
                repo.Commit();
                repo.Dispose();
            }
        }

        //Subscription Type
        public List<SubscriptionTypeDto> GetSubscriptionType()
        {
            using (var repo = new EfUnitOfWork())
            {
                var lst = new List<SubscriptionTypeDto>();
                var data = repo.SubRepository.All;
                foreach (var item in data)
                {
                    lst.Add(new SubscriptionTypeDto
                    {
                        Id = item.Id,
                        SubscriptionTypeTitle = item.SubscriptionTypeTitle
                    });
                }
                repo.Commit();
                repo.Dispose();
                return lst;
            }
        }

        //User Subscription
        public List<UserSubscriptionDto> GetUserSubscription()
        {
            using (var repo = new EfUnitOfWork())
            {
                var lst = new List<UserSubscriptionDto>();
                var data = repo.UsersubRepository.All;
                foreach (var item in data)
                {
                    lst.Add(new UserSubscriptionDto
                    {
                        Id = item.Id,
                        Title = item.Title
                    });
                }
                repo.Commit();
                repo.Dispose();
                return lst;
            }
        }

        //Content Details
        public List<ContentDetailsDto> GetContentDetail()
        {
            using (var repo = new EfUnitOfWork())
            {
                var lst = new List<ContentDetailsDto>();
                var data = repo.ContentdtlRepository.All;
                foreach (var item in data)
                {
                    lst.Add(new ContentDetailsDto
                    {
                        Id = item.Id,
                        ContentId = item.ContentId
                    });
                }
                repo.Commit();
                repo.Dispose();
                return lst;
            }
        }       

        //Price Matrix
        public List<PriceMatrixDto> GetPriceMatrix()
        {
            using (var repo = new EfUnitOfWork())
            {
                var lst = new List<PriceMatrixDto>();
                var data = repo.PriceMatrixRepository.All;
                foreach (var item in data)
                {
                    lst.Add(new PriceMatrixDto
                    {
                        Id = item.Id,
                        Price = item.Price,
                        SubscriptionTypeId = item.SubscriptionTypeId
                    });
                }
                repo.Commit();
                repo.Dispose();
                return lst;
            }
        }

        public void AddPriceMatrix(PriceMatrixDto price)
        {
            using (var repo = new EfUnitOfWork())
            {
                var subDetail = new PriceMatrix()
                {
                    Id = price.Id,
                    Price = price.Price,
                    SubscriptionTypeId = price.SubscriptionTypeId
                };
                repo.PriceMatrixRepository.InsertOrUpdate(subDetail);
                repo.Commit();
            }
        }

        public PriceMatrixDto GetPriceMatrixById(int priceMatrixId)
        {
            using (var repo = new EfUnitOfWork())
            {
                var item = repo.PriceMatrixRepository.FindMany(s => s.Id == priceMatrixId).FirstOrDefault();
                return new PriceMatrixDto
                {
                    Id = item.Id,
                    Price = item.Price,
                    SubscriptionTypeId = item.SubscriptionTypeId
                };
            }
        }

        public void UpdatePriceMatrix(PriceMatrixDto price)
        {
            using (var repo = new EfUnitOfWork())
            {
                repo.PriceMatrixRepository.InsertOrUpdate(new PriceMatrix
                {
                    Id = price.Id,
                    Price = price.Price,
                    SubscriptionTypeId = price.SubscriptionTypeId
                });
                repo.Commit();
                repo.Dispose();
            }
        }

        public void DeletePriceMatrix(int priceMatrixId)
        {
            using (var repo = new EfUnitOfWork())
            {
                repo.PriceMatrixRepository.Delete(priceMatrixId);
                repo.Commit();
                repo.Dispose();
            }
        }

        //Professor Details
        public List<ProfessorDto> GetProfessor()
        {
            using (var repo = new EfUnitOfWork())
            {
                var lst = new List<ProfessorDto>();
                var data = repo.ProfessorDetailsRepository.All;
                foreach (var item in data)
                {
                    lst.Add(new ProfessorDto
                    {
                        Id = item.Id,
                        ProfessorName = item.ProfessorName,
                        Profile = item.Profile,
                        ProfessorImg = item.ProfessorImg
                    });
                }
                repo.Commit();
                repo.Dispose();
                return lst;
            }
        }

        public void AddProfessor(ProfessorDto prof)
        {
            using (var repo = new EfUnitOfWork())
            {
                var subDetail = new ProfessorDetails()
                {
                    Id = prof.Id,
                    ProfessorName = prof.ProfessorName,
                    Profile = prof.Profile,
                    ProfessorImg = prof.ProfessorImg
                };
                repo.ProfessorDetailsRepository.InsertOrUpdate(subDetail);
                repo.Commit();
            }
        }

        public ProfessorDto GetProfessorById(int professorId)
        {
            using (var repo = new EfUnitOfWork())
            {
                var item = repo.ProfessorDetailsRepository.FindMany(s => s.Id == professorId).FirstOrDefault();
                return new ProfessorDto
                {
                    Id = item.Id,
                    ProfessorName = item.ProfessorName,
                    Profile = item.Profile,
                    ProfessorImg = item.ProfessorImg
                };
            }
        }

        public void UpdateProfessor(ProfessorDto prof)
        {
            using (var repo = new EfUnitOfWork())
            {
                repo.ProfessorDetailsRepository.InsertOrUpdate(new ProfessorDetails
                {
                    Id = prof.Id,
                    ProfessorName = prof.ProfessorName,
                    Profile = prof.Profile,
                    ProfessorImg = prof.ProfessorImg
                });
                repo.Commit();
                repo.Dispose();
            }
        }

        public void DeleteProfessor(int professorId)
        {
            using (var repo = new EfUnitOfWork())
            {
                repo.ProfessorDetailsRepository.Delete(professorId);
                repo.Commit();
                repo.Dispose();
            }
        }
    }
}
