﻿using Education.DataAccess;
using Education.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Service
{
   public  class OrderService
    {
       public void InsertOrderDetails(int UserId, int Amount, int UserSubsId)
       {

           try
           {
               int OrderID = (int)DateTime.Now.Ticks;
               Random rId = new Random(OrderID);

               using (var repo = new EfUnitOfWork())
               {
                   var Order = new Orders
                   {
                       OrderId = rId.Next(),
                       UserId = UserId,
                       OrderDate = DateTime.Today,
                       Amount = Amount,

                   };
                   repo.OrderRepository.InsertOrUpdate(Order);
                   repo.Commit();


                   var OrderDtls = new Order_Details
                   {
                       OrderId = Order.OrderId,
                       UserSubscriptionId = UserSubsId,
                       CreateDate = Order.OrderDate
                   };
                   repo.OrderDetailsRepository.InsertOrUpdate(OrderDtls);
                   repo.Commit();
                   repo.Dispose();
               }
           }
           catch (Exception ex)
           {
               //Code for exception handling
           }
          


       }
    }
}
