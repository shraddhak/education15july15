﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Education.Dtos;
using Education.DataAccess;
using Education.Master;
using System.Data;
using System.Web;

namespace Education.Service
{
    public class UserService
    {
        public void AddUser(UserDto userDto)
        {
            try
            {
                User user = new User() { Name = userDto.Name, Email = userDto.Email };

                EfUnitOfWork efUnitOfWork = new EfUnitOfWork();

                efUnitOfWork.UserRepository.InsertOrUpdate(user);


                efUnitOfWork.Commit();

                efUnitOfWork.Dispose();

            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
           
        }

        public void AddBranch(string branchName)
        {
            using (var repo = new EfUnitOfWork())
            {
                var branch = new Branch() { BrachTitle = branchName };
                repo.BranchRepository.InsertOrUpdate(branch);
                repo.Commit();
                repo.Dispose();
            }

        }

        public List<UserDto> GetUser()
        {
            var lst = new List<UserDto>();
                
            try
            {
                 using (var repo = new EfUnitOfWork())
            {
                var data = repo.UserRepository.FindMany(s => s.Name != null).ToList();
                foreach (var item in data)
                {
                    lst.Add(new UserDto { Id = item.Id, Name = item.Name });
                }
                repo.Commit();
                repo.Dispose();
                
            }
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
            return lst; 
        }

        public List<BranchDto> GetBranch()
        {
            var lst = new List<BranchDto>();
            try
            {
                using (var repo = new EfUnitOfWork())
                {
                   
                    var data = repo.BranchRepository.FindMany(s => s.BrachTitle != null).ToList();
                    foreach (var item in data)
                    {
                        lst.Add(new BranchDto { Id = item.Id, BrachTitle = item.BrachTitle });
                    }
                    repo.Commit();
                    repo.Dispose();
                    
                }
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
           return lst;


        }
        /// <summary>
        /// Get year
        /// </summary>
        /// <returns></returns>
        public List<YearDto> GetYear()
        {
            using (var repo = new EfUnitOfWork())
            {
                var lst = new List<YearDto>();
                var data = repo.YearRepository.FindMany(s => s.YearTitle != null).ToList();
                foreach (var item in data)
                {
                    lst.Add(new YearDto { Id = item.Id, YearTitle = item.YearTitle });
                }
                repo.Commit();
                repo.Dispose();
                return lst;
            }
        }

        /// <summary>
        /// Get Semester
        /// </summary>
        /// <returns></returns>
        public List<SemesterDto> GetSemester()
        {
            var lst = new List<SemesterDto>();
            try
            {
                using (var repo = new EfUnitOfWork())
                {
                   
                    var data = repo.SemesterRepository.FindMany(s => s.SemesterTitle != null).ToList();
                    foreach (var item in data)
                    {
                        lst.Add(new SemesterDto { Id = item.Id, SemesterTitle = item.SemesterTitle });
                    }
                    repo.Commit();
                    repo.Dispose();
                   
                }
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
            return lst;
        }

        /// <summary>
        /// Get Subject
        /// </summary>
        /// <returns></returns>
        public List<SubjectDto> GetSubject()
        {
            var lst = new List<SubjectDto>();
            try
            {
                using (var repo = new EfUnitOfWork())
                {
                   
                    var data = repo.SubjectRepository.FindMany(s => s.IsRemoved == true).ToList();
                    foreach (var item in data)
                    {
                        lst.Add(new SubjectDto { Id = item.Id, SubjectTitle = item.SubjectTitle });
                    }
                    repo.Commit();
                    repo.Dispose();
                    
                }
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
            return lst;
            
        }
        /// <summary>
        /// Get Chapters
        /// </summary>
        /// <returns></returns>
        public List<ChapterDto> GetChapter()
        {
            var lst = new List<ChapterDto>();
            try
            {
                using (var repo = new EfUnitOfWork())
                {
                   
                    var data = repo.ChapterRepository.FindMany(s => s.SubjectId == 6).ToList();
                    foreach (var item in data)
                    {
                        lst.Add(new ChapterDto { Id = item.Id, Title = item.Title });
                    }
                    repo.Commit();
                    repo.Dispose();
                  
                }
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
            return lst; 

        }

        
        public void RegisterUser(UserDto userDto)
        {
            try
            {
                using (var repo = new EfUnitOfWork())
                {
                    var user = new User
                    {
                        Name = userDto.Name,
                        Address = userDto.Address,
                        Email = userDto.Email,
                        MobileNo = userDto.MobileNo,
                        Password = userDto.Password
                    };

                    repo.UserRepository.InsertOrUpdate(user);
                    repo.Commit();
                    repo.Dispose();

                }
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
            
        }

        
        /// <summary>
        /// Add data to user subscription
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public int AddUserSubscription(UserSubscriptionDto dto)
        {
            int id = 0;
            try
            {
                using (var repo = new EfUnitOfWork())
                {
                    var user = new UserSubcription
                    {
                        UserId = dto.UserId,
                        SubscriptionTypeId = dto.SubscriptionTypeId

                    };

                    repo.UsersubRepository.InsertOrUpdate(user);
                    repo.Commit();
                    repo.Dispose();

                    id = user.Id;
                }
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
            return id;

        }

        /// <summary>
        /// Add data to temp table
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public int AddTempUserSubscription(Temp_UserSubscriptionDto dto)
        {
            int id = 0;
            try
            {
                using (var repo = new EfUnitOfWork())
                {
                    var user = new Temp_UserSubscription
                    {
                        UserId = dto.UserId,
                        SubscriptionTypeId = dto.SubscriptionTypeId,
                        Sessionid = dto.Sessionid
                    };

                    repo.Temp_UserSubscriptionRepository.InsertOrUpdate(user);
                    repo.Commit();
                    repo.Dispose();

                    id = user.Id;
                }
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
            return id;

        }

        public UserDto IsPresent(string name, string pass)
        {
            var test = new UserDto();
            try
            {
                using (var repo = new EfUnitOfWork())
                {
                   
                    var res = repo.UserRepository.FindMany(s => s.Name == name && s.Password == pass);
                    foreach (var item in res)
                    {
                        test.Name = item.Name;
                        test.Id = item.Id;
                    }
                   
                }
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
            return test;
        }





        public List<DashboardDto> GetListByTempCntId(int uid, string sessionId)
        {
            List<DashboardDto> cntList = new List<DashboardDto>();
            DashboardDto dashdto = new DashboardDto();
            List<SemesterDto> semDto = new List<SemesterDto>();
            List<ChapterDto> chapeDto = new List<ChapterDto>();
            List<SubjectDto> subjDto = new List<SubjectDto>();
            List<LectureDto> lectDto = new List<LectureDto>();
            int contId = 0;
            try
            {
                using (var repo = new EfUnitOfWork())
                {
                    var SubscriptionTypeId = repo.Temp_UserSubscriptionRepository.FindMany(s => s.Sessionid == sessionId && s.UserId == uid && s.Isprocess == false && s.IsRemoved == false).ToList();
                    foreach (var sid in SubscriptionTypeId)
                    {
                        var SubscriptionTypeTitle = repo.SubRepository.FindMany(s => s.Id == sid.SubscriptionTypeId).Select(s => s.SubscriptionTypeTitle).ToList();
                        foreach (string a in SubscriptionTypeTitle) { dashdto.SubscriptionTypeTitle = a; }
                        var TempUsrSubid = repo.Temp_SubscriptionDetailsRepository.FindMany(s => s.TempUserSubscriptionId == sid.Id && s.Isprocess == false && s.IsRemoved == false).ToList();
                        foreach (var Tusrid in TempUsrSubid)
                        {
                            contId = Tusrid.ContentId;
                            var TempCntDtls = repo.Temp_ContentDetailsRepository.FindMany(s => s.TempSubscriptionDetailId == Tusrid.Id && s.Isprocess == false && s.IsRemoved == false).ToList();

                            foreach (var tempcnt in TempCntDtls)
                            {
                                DateTime sDate = DateTime.Now; //Convert.ToDateTime(tempcnt.StartDate);
                                DateTime eDate = Convert.ToDateTime(tempcnt.EndDate);

                                string diff1;
                                TimeSpan span = eDate.Subtract(sDate);
                                string hour = span.Hours.ToString();
                                string days = span.Days.ToString();

                                // if (!(days == 0 && hour == 0))  //
                                // {
                                diff1 = "Days: " + span.Days.ToString() + " Hours: " + span.Hours.ToString();                //  TotalDays.ToString() + " Days ";    // +TotalHours.ToString() + " Hours";
                                //} 
                                //else
                                // { 
                                //     diff1 = "-"; 
                                //}

                                switch (contId)
                                {
                                    case 1:
                                        var semSearch = repo.SemesterRepository.FindMany(s => s.Id == tempcnt.ContentId).ToList();
                                        var subCnt = repo.SubjectRepository.FindMany(s => s.SemesterId == tempcnt.ContentId).ToList();
                                        int PriceMatrix = repo.PriceMatrixRepository.FindMany(s => s.SubscriptionTypeId == 1).Select(s => s.Price).FirstOrDefault();
                                        int SemPrice = subCnt.Count() * PriceMatrix;
                                        foreach (var st in semSearch)
                                        {

                                            semDto.Add(new SemesterDto() { Id = st.Id, SemesterTitle = st.SemesterTitle, CreatedDate = tempcnt.StartDate, EndDate = tempcnt.EndDate, DateDiff = diff1, TempContentId = tempcnt.Id, Price = SemPrice });
                                        }

                                        break;

                                    case 3:
                                        var chapSearch = repo.ChapterRepository.FindMany(s => s.Id == tempcnt.ContentId).ToList();

                                        foreach (var st in chapSearch)
                                        {
                                            var lectCount = repo.LectureRepository.FindMany(s => s.ChapterId == st.Id).ToList();
                                            int PriceMatrixChap = repo.PriceMatrixRepository.FindMany(s => s.SubscriptionTypeId == 3).Select(s => s.Price).FirstOrDefault();
                                            int chapPrice = lectCount.Count() * PriceMatrixChap;

                                            chapeDto.Add(new ChapterDto() { Id = st.Id, Title = st.Title, CreatedDate = tempcnt.StartDate, EndDate = tempcnt.EndDate, DateDiff = diff1, TempContentId = tempcnt.Id, Price = chapPrice });

                                            var lecture = repo.LectureRepository.FindMany(s => s.ChapterId == st.Id).ToList();
                                            foreach (var lt in lecture)
                                            {
                                                lectDto.Add(new LectureDto { LectureTitle = lt.LectureTitle, VideoURL = lt.VideoURL });
                                            }
                                        }

                                        break;

                                    case 2:
                                        var subSearch = repo.SubjectRepository.FindMany(s => s.Id == tempcnt.ContentId).ToList();
                                        int PriceMatrixSubject = repo.PriceMatrixRepository.FindMany(s => s.SubscriptionTypeId == 2).Select(s => s.Price).FirstOrDefault();

                                        foreach (var st in subSearch)
                                        {
                                            subjDto.Add(new SubjectDto() { Id = st.Id, SubjectTitle = st.SubjectTitle, CreatedDate = tempcnt.StartDate, EndDate = tempcnt.EndDate, DateDiff = diff1, TempContentId = tempcnt.Id, Price = PriceMatrixSubject });
                                        }

                                        break;

                                }
                            }

                        }

                    }
                    dashdto.ContentId = contId;
                    dashdto.SDto = semDto;
                    dashdto.chapDto = chapeDto;
                    dashdto.lectDto = lectDto;
                    dashdto.subDto = subjDto;

                }
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
         
            cntList.Add(dashdto);
            return cntList;
        }

        public List<DashboardDto> GetContentByTempCntId(int uid)
        {
            List<DashboardDto> cntList = new List<DashboardDto>();
            DashboardDto dashdto = new DashboardDto();
            List<SemesterDto> semDto = new List<SemesterDto>();
            List<ChapterDto> chapeDto = new List<ChapterDto>();
            List<SubjectDto> subjDto = new List<SubjectDto>();
            List<LectureDto> lectDto = new List<LectureDto>();
            int contId = 0;
            try
            {
                using (var repo = new EfUnitOfWork())
                {
                    var SubscriptionTypeId = repo.Temp_UserSubscriptionRepository.FindMany(s => s.UserId == uid).ToList();
                    foreach (var sid in SubscriptionTypeId)
                    {
                        var SubscriptionTypeTitle = repo.SubRepository.FindMany(s => s.Id == sid.SubscriptionTypeId).Select(s => s.SubscriptionTypeTitle).ToList();
                        foreach (string a in SubscriptionTypeTitle) { dashdto.SubscriptionTypeTitle = a; }
                        var TempUsrSubid = repo.Temp_SubscriptionDetailsRepository.FindMany(s => s.TempUserSubscriptionId == sid.Id).ToList();
                        foreach (var Tusrid in TempUsrSubid)
                        {
                            contId = Tusrid.ContentId;
                            var TempCntDtls = repo.Temp_ContentDetailsRepository.FindMany(s => s.TempSubscriptionDetailId == Tusrid.Id).ToList();
                            foreach (var tempcnt in TempCntDtls)
                            {

                                switch (contId)
                                {
                                    case 1:
                                        var semSearch = repo.SemesterRepository.FindMany(s => s.Id == tempcnt.ContentId).ToList();
                                        foreach (var st in semSearch)
                                        {
                                            // semDto.Add(new SemesterDto() { Id = st.Id, SemesterTitle = st.SemesterTitle, CreatedDate = sid.CreateDate });
                                        }

                                        break;

                                    case 3:
                                        var chapSearch = repo.ChapterRepository.FindMany(s => s.Id == tempcnt.ContentId).ToList();
                                        foreach (var st in chapSearch)
                                        {
                                            // chapeDto.Add(new ChapterDto() { Id = st.Id, Title = st.Title, CreatedDate = sid.CreateDate });
                                            var lecture = repo.LectureRepository.FindMany(s => s.ChapterId == st.Id).ToList();
                                            foreach (var lt in lecture)
                                            {
                                                lectDto.Add(new LectureDto { LectureTitle = lt.LectureTitle, VideoURL = lt.VideoURL });
                                            }
                                        }

                                        break;

                                    case 2:
                                        var subSearch = repo.SubjectRepository.FindMany(s => s.Id == tempcnt.ContentId).ToList();
                                        foreach (var st in subSearch)
                                        {
                                            //subjDto.Add(new SubjectDto() { Id = st.Id, SubjectTitle = st.SubjectTitle, CreatedDate = sid.CreateDate });
                                        }
                                        break;
                                }
                            }
                            break;
                        }
                        break;
                    }
                    dashdto.ContentId = contId;
                    dashdto.SDto = semDto;
                    dashdto.chapDto = chapeDto;
                    dashdto.lectDto = lectDto;
                    dashdto.subDto = subjDto;

                }
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
        
            cntList.Add(dashdto);
            return cntList;
        }

        public List<SubjectDto> GetAllSubjectsBySemesterId(int SemesterId)
        {
            var lst = new List<SubjectDto>();
            try
            {
                using (var repo = new EfUnitOfWork())
                {
                   
                    var data = repo.SubjectRepository.FindMany(s => s.SubjectTitle != null).Where(s => s.SemesterId == SemesterId).ToList();
                    foreach (var item in data)
                    {
                        lst.Add(new SubjectDto { Id = item.Id, SubjectTitle = item.SubjectTitle });
                    }
                    repo.Commit();
                    repo.Dispose();
                   
                }
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
            return lst;
        }

        public List<ChapterDto> GetAllChaptersBySubjectId(int SubjectId)
        {
            var lst = new List<ChapterDto>();
            try
            {
                using (var repo = new EfUnitOfWork())
                {
                   
                    var data = repo.ChapterRepository.FindMany(s => s.Title != null).Where(s => s.SubjectId == SubjectId).ToList();
                    foreach (var item in data)
                    {
                        lst.Add(new ChapterDto { Id = item.Id, Title = item.Title });
                    }
                    repo.Commit();
                    repo.Dispose();
                   
                }
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
            return lst; 
        }


  



        /// <summary>
        /// Get Subject
        /// </summary>
        /// <returns></returns>
        public List<DashboardDto> GetSubject1(int itemid)
        {
            DashboardDto dashdto = new DashboardDto();        
            List<DashboardDto> cntList = new List<DashboardDto>();
            try
            {
                using (var repo = new EfUnitOfWork())
                {
                    var subjectlst = new List<SubjectDto>();
                    var chapterlst = new List<ChapterDto>();
                    var lecturelist = new List<LectureDto>();

                    var Subjectdata = repo.SubjectRepository.FindMany(s => s.SemesterId == itemid).ToList();

                    foreach (var item in Subjectdata)
                    {
                        subjectlst.Add(new SubjectDto { Id = item.Id, SubjectTitle = item.SubjectTitle });
                        var ChapterData = repo.ChapterRepository.FindMany(s => s.SubjectId == item.Id).ToList();

                        foreach (var item1 in ChapterData)
                        {
                            chapterlst.Add(new ChapterDto { Id = item1.Id, Title = item1.Title, SubjectId = item1.SubjectId });

                            var LectureData = repo.LectureRepository.FindMany(s => s.ChapterId == item1.Id).ToList();

                            foreach (var item2 in LectureData)
                            {
                                lecturelist.Add(new LectureDto { Id = item2.Id, LectureTitle = item2.LectureTitle, ChapterId = item2.ChapterId, VideoURL = item2.VideoURL });

                            }

                        }

                    }

                    dashdto.subDto = subjectlst;
                    dashdto.chapDto = chapterlst;
                    dashdto.lectDto = lecturelist;
                    cntList.Add(dashdto);

                    
                }  
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
            return cntList;               
        }


        /// <summary>
        /// Get Chapters
        /// </summary>
        /// <returns></returns>
        public List<DashboardDto> GetChapter1(int itemid)
        {
            DashboardDto dashdto = new DashboardDto();
            List<DashboardDto> cntList = new List<DashboardDto>();
            var lecturelist = new List<LectureDto>();
            try
            {
                using (var repo = new EfUnitOfWork())
                {
                    var chapterlst = new List<ChapterDto>();
                    var data = repo.ChapterRepository.FindMany(s => s.SubjectId == itemid).ToList();

                    foreach (var item in data)
                    {
                        chapterlst.Add(new ChapterDto { Id = item.Id, Title = item.Title });

                        var LectureData = repo.LectureRepository.FindMany(s => s.ChapterId == item.Id).ToList();

                        foreach (var item2 in LectureData)
                        {
                            lecturelist.Add(new LectureDto { Id = item2.Id, LectureTitle = item2.LectureTitle, ChapterId = item2.ChapterId, VideoURL = item2.VideoURL });

                        }
                    }

                    dashdto.chapDto = chapterlst;
                    dashdto.lectDto = lecturelist;
                    cntList.Add(dashdto);

                    
                }
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
            return cntList;
        }





        public List<DashboardDto> GetLecture1(int itemid)
        {
            DashboardDto dashdto = new DashboardDto();
            List<DashboardDto> cntList = new List<DashboardDto>();
            var lecturelist = new List<LectureDto>();
            try
            {
                using (var repo = new EfUnitOfWork())
                {
                    var LectureData = repo.LectureRepository.FindMany(s => s.ChapterId == itemid).ToList();

                    foreach (var item2 in LectureData)
                    {
                        lecturelist.Add(new LectureDto { Id = item2.Id, LectureTitle = item2.LectureTitle, ChapterId = item2.ChapterId, VideoURL = item2.VideoURL });

                    }

                    dashdto.lectDto = lecturelist;
                    cntList.Add(dashdto);
                    //repo.Commit();
                    //repo.Dispose();
                    
                }
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }

            return cntList;

        }


        public void InsertDataIntoFinalTbl(int id)
        {
            try
            {
                using (var repo = new EfUnitOfWork())
                {

                    var UsrSubslst = new List<UserSubscriptionDto>();
                    var SubsDtllst = new List<SubscriptionDetailDto>();
                    var Cntdtllst = new List<ContentDetailsDto>();

                    var UsrSubsData = repo.Temp_UserSubscriptionRepository.FindMany(s => s.UserId == id);
                    foreach (var item1 in UsrSubsData)
                    {
                        UsrSubslst.Add(new UserSubscriptionDto
                        {
                            UserId = item1.UserId,
                            SubscriptionTypeId = item1.SubscriptionTypeId,
                            CreateDate = item1.CreateDate,
                            UpdateDate = item1.UpdateDate,
                            IsRemoved = item1.IsRemoved
                        });

                    }


                    var Subsdata = repo.Temp_SubscriptionDetailsRepository.FindMany(s => s.UserId == id);
                    foreach (var item in Subsdata)
                    {
                        SubsDtllst.Add(new SubscriptionDetailDto
                        {
                            Id = item.Id,
                            UserId = item.UserId,
                            SubscriptionTypeId = item.SubscriptionTypeId,
                            UserSubscriptionId = item.TempUserSubscriptionId,
                            ContentId = item.ContentId,
                            StartTime = item.StartTime,
                            EndTime = item.EndTime,
                            CreateDate = item.CreateDate,
                            UpdateDate = item.UpdateDate,
                            IsRemoved = item.IsRemoved
                        });

                        var cntDtldata = repo.Temp_ContentDetailsRepository.FindMany(s => s.TempSubscriptionDetailId == item.Id);
                        foreach (var item2 in cntDtldata)
                        {
                            Cntdtllst.Add(new ContentDetailsDto
                            {
                                ContentId = item2.ContentId,
                                SubscriptionDetailId = item2.TempSubscriptionDetailId,
                                StartDate = item2.StartDate,
                                EndDate = item2.EndDate,
                                CreateDate = item2.CreateDate,
                                UpdateDate = item2.UpdateDate,
                                IsRemoved = item2.IsRemoved

                            });
                        }
                    }
                    repo.Commit();
                }
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }

        }

        public int AddSubscriptionDetails(SubscriptionDetailDto subDto)
        {
            int id;
            using (var repo = new EfUnitOfWork())
            {


                var sub = new SubscriptionDetails
                {
                    UserId = subDto.UserId,
                    SubscriptionTypeId = subDto.SubscriptionTypeId,
                    StartTime = subDto.StartTime,
                    EndTime = subDto.EndTime,
                    ContentId = subDto.ContentId,

                };

                repo.SubdtlRepository.InsertOrUpdate(sub);
                repo.Commit();
                repo.Dispose();

                id = sub.Id;


            }

            return id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="subDto"></param>
        /// <returns></returns>
        public int AddTempSubscriptionDetails(Temp_SubscriptionDetailsDto subDto)
        {
            int id;
            using (var repo = new EfUnitOfWork())
            {


                var sub = new Temp_SubscriptionDetails
                {
                    UserId = subDto.UserId,
                    SubscriptionTypeId = subDto.SubscriptionTypeId,
                    TempUserSubscriptionId = subDto.TempUserSubscriptionId,
                    StartTime = subDto.StartTime,
                    EndTime = subDto.EndTime,
                    ContentId = subDto.ContentId,

                };


                repo.Temp_SubscriptionDetailsRepository.InsertOrUpdate(sub);
                repo.Commit();
                repo.Dispose();

                id = sub.Id;


            }

            return id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public void AddContentDeatils(ContentDetailsDto dto)
        {
            try
            {
                using (var repo = new EfUnitOfWork())
                {

                    var cont = new ContentDetails
                    {
                        ContentId = dto.ContentId,
                        SubscriptionDetailId = dto.SubscriptionDetailId,
                        StartDate = dto.StartDate,
                        EndDate = dto.EndDate
                    };

                    repo.ContentdtlRepository.InsertOrUpdate(cont);
                    repo.Commit();
                    repo.Dispose();

                }
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
            

        }

        public void AddTempContentDeatils(Temp_ContentDetailsDto dto)
        {
            try
            {
                using (var repo = new EfUnitOfWork())
                {

                    var cont = new Temp_ContentDetails
                    {
                        ContentId = dto.ContentId,
                        TempSubscriptionDetailId = dto.TempSubscriptionDetailId,
                        StartDate = dto.StartDate,
                        EndDate = dto.EndDate
                    };

                    repo.Temp_ContentDetailsRepository.InsertOrUpdate(cont);
                    repo.Commit();
                    repo.Dispose();

                }
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
            

        }


        public List<SubscriptionTypeDto> GetSubTypes()
        {
            var lst = new List<SubscriptionTypeDto>();
            try
            {
                using (var repo = new EfUnitOfWork())
                {
                   
                    var data = repo.SubRepository.FindMany(s => s.SubscriptionTypeTitle != null).ToList();
                    foreach (var item in data)
                    {
                        lst.Add(new SubscriptionTypeDto { Id = item.Id, SubscriptionTypeTitle = item.SubscriptionTypeTitle });
                    }
                    repo.Commit();
                    repo.Dispose();
                   
                }
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
            return lst; 
        }

        public List<SubDetailsDto> GetSubscriberDetails(int userId)
        {
            using (var repo = new EfUnitOfWork())
            {
                var model = new List<SubDetailsDto>();

                var subDetails = repo.SubdtlRepository.FindMany(s => s.UserId == userId).ToList();

                foreach (var item in subDetails)
                {
                    var subType = repo.SubRepository.FindMany(s => s.Id == item.SubscriptionTypeId).Select(s => s.SubscriptionTypeTitle);
                }
                // var subType = repo.SubRepository.FindMany(s=>s.Id == subDetails.).Select(s=>s.SubscriptionTypeTitle);

                //var subject = repo.SubjectRepository.FindMany(s=>s.Id == subDetails.ContentId);


                return model;
            }
        }

        public List<ContentDetailsDto> GetContent(int Subdtlid)
        {
            var model = new List<ContentDetailsDto>();
            try
            {
                using (var repo = new EfUnitOfWork())
                { 
                    var dtl = repo.Temp_UserSubscriptionRepository.FindMany(s => s.UserId == Subdtlid).Select(s => s.SubscriptionTypeId).ToList();

                }
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
            return model;


        }

        public List<string> getu(int uid)
        {
            System.Collections.Generic.List<string> subType = new List<string>();
            try
            {
                using (var repo = new EfUnitOfWork())
                {

                    var sid = repo.Temp_UserSubscriptionRepository.FindMany(s => s.UserId == uid).ToList();
                    foreach (var item in sid)
                    {
                        //int suid = Convert.ToInt16(item.SubscriptionTypeId);
                        var res = repo.SubRepository.FindMany(s => s.Id == item.SubscriptionTypeId).ToList();

                        foreach (var item1 in res)
                        {
                            subType.Add(item1.SubscriptionTypeTitle);
                        }

                    }
                    
                }
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
            return subType;
        }

        public void DeleteSubscribtion(int SubTypId, int UserId, int ContentId)
        {
            List<int> list = new List<int>();
            List<int> Userlist = new List<int>();
            try
            {
                using (var repo = new EfUnitOfWork())
                {

                    var TempCntDtls = repo.Temp_ContentDetailsRepository.FindMany(s => s.Id == ContentId).FirstOrDefault();

                    var TempSubDtls = repo.Temp_ContentDetailsRepository.FindMany(s => s.TempSubscriptionDetailId == TempCntDtls.TempSubscriptionDetailId).ToList();

                    TempCntDtls.IsRemoved = true;
                    repo.Temp_ContentDetailsRepository.InsertOrUpdate(TempCntDtls);
                    repo.Commit();

                    //foreach (var itenc in TempCntDtls)
                    //{
                    //    itenc.IsRemoved = true;
                    //    repo.Temp_ContentDetailsRepository.InsertOrUpdate(itenc);
                    //    repo.Commit();
                    //}
                    // repo.Temp_ContentDetailsRepository.Delete(ContentId);

                    if (TempSubDtls.Count() == 1)
                    {

                        int TempsubDetailId = TempCntDtls.TempSubscriptionDetailId;

                        foreach (var itenc in TempSubDtls)
                        {
                            itenc.IsRemoved = true;
                            repo.Temp_ContentDetailsRepository.InsertOrUpdate(itenc);
                            repo.Commit();
                        }
                        //repo.Temp_ContentDetailsRepository.Delete(ContentId);

                        var TempUsrSubid = repo.Temp_SubscriptionDetailsRepository.FindMany(s => s.Id == TempsubDetailId).FirstOrDefault();

                        int tempUsersubId = TempUsrSubid.TempUserSubscriptionId;
                        TempUsrSubid.IsRemoved = true;
                        repo.Temp_SubscriptionDetailsRepository.InsertOrUpdate(TempUsrSubid);
                        //repo.Temp_SubscriptionDetailsRepository.Delete(TempsubDetailId);

                        var UserSubcribtion = repo.Temp_UserSubscriptionRepository.FindMany(s => s.Id == tempUsersubId).FirstOrDefault();
                        UserSubcribtion.IsRemoved = true;
                        repo.Temp_UserSubscriptionRepository.InsertOrUpdate(UserSubcribtion);
                        repo.Commit();
                        //repo.Temp_UserSubscriptionRepository.Delete(tempUsersubId);
                    }


                }
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }


        }

        public string GetEmailByUsrId(int id)
        {
            string userEmail = string.Empty;
            try
            {
                using (var repo = new EfUnitOfWork())
                {
                    userEmail = repo.UserRepository.FindMany(s => s.Id == id).Select(s => s.Email).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
            
            return userEmail;
        }

        public void InsertSessionData(Education.Dtos.UserSession objUser)
        {
            try
            {
                using (var repo = new EfUnitOfWork())
                {
                    var userSession = new Education.Master.UserSession
                    {
                        UserId = objUser.UserId,
                        OTP = objUser.OTP,
                        UserSessionID = objUser.UserSessionID,
                    };

                    repo.UserSessionRepository.InsertOrUpdate(userSession);
                    repo.Commit();
                    repo.Dispose();
                    HttpContext.Current.Session["UserSessionId"] = objUser.UserSessionID;
                   
                }


            }
            catch
            { 
                throw; 
            }

      
        }


        public string GetUserName(string EmailId)
        {
            string UserName = "";
            try
            {
                var s = EmailId.Split('@');
                UserName = s[0];
                
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
            return UserName;
        }
       
        
    }
}
