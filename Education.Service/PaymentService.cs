﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Education.Dtos;
using Education.DataAccess;
using Education.Master;


namespace Education.Service
{
    public class PaymentService
    {
        OrderService orderService = new OrderService();
        public List<DashboardDto> GetListByTempCntId(int uid)
        {
            List<DashboardDto> cntList = new List<DashboardDto>();
            try
            {
             
                DashboardDto dashdto = new DashboardDto();
                List<SemesterDto> semDto = new List<SemesterDto>();
                List<ChapterDto> chapeDto = new List<ChapterDto>();
                List<SubjectDto> subjDto = new List<SubjectDto>();
                List<LectureDto> lectDto = new List<LectureDto>();
                int contId = 0;
                using (var repo = new EfUnitOfWork())
                {
                    var SubscriptionTypeId = repo.Temp_UserSubscriptionRepository.FindMany(s => s.UserId == uid).ToList();
                    foreach (var sid in SubscriptionTypeId)
                    {
                        var SubscriptionTypeTitle = repo.SubRepository.FindMany(s => s.Id == sid.SubscriptionTypeId).Select(s => s.SubscriptionTypeTitle).ToList();
                        foreach (string a in SubscriptionTypeTitle) { dashdto.SubscriptionTypeTitle = a; }
                        var TempUsrSubid = repo.Temp_SubscriptionDetailsRepository.FindMany(s => s.TempUserSubscriptionId == sid.Id).ToList();
                        foreach (var Tusrid in TempUsrSubid)
                        {
                            contId = Tusrid.ContentId;
                            var TempCntDtls = repo.Temp_ContentDetailsRepository.FindMany(s => s.TempSubscriptionDetailId == Tusrid.Id).ToList();

                            foreach (var tempcnt in TempCntDtls)
                            {
                                DateTime sDate = DateTime.Now; //Convert.ToDateTime(tempcnt.StartDate);
                                DateTime eDate = Convert.ToDateTime(tempcnt.EndDate);

                                string diff1;
                                TimeSpan span = eDate.Subtract(sDate);
                                string hour = span.Hours.ToString();
                                string days = span.Days.ToString();

                                // if (!(days == 0 && hour == 0))  //
                                // {
                                diff1 = "Days: " + span.Days.ToString() + " Hours: " + span.Hours.ToString();                //  TotalDays.ToString() + " Days ";    // +TotalHours.ToString() + " Hours";
                                //} 
                                //else
                                // { 
                                //     diff1 = "-"; 
                                //}

                                switch (contId)
                                {
                                    case 1:
                                        var semSearch = repo.SemesterRepository.FindMany(s => s.Id == tempcnt.ContentId).ToList();
                                        var subCnt = repo.SubjectRepository.FindMany(s => s.SemesterId == tempcnt.ContentId).ToList();
                                        int PriceMatrix = repo.PriceMatrixRepository.FindMany(s => s.SubscriptionTypeId == 1).Select(s => s.Price).FirstOrDefault();
                                        int SemPrice = subCnt.Count() * PriceMatrix;
                                        foreach (var st in semSearch)
                                        {

                                            semDto.Add(new SemesterDto() { Id = st.Id, SemesterTitle = st.SemesterTitle, CreatedDate = tempcnt.StartDate, EndDate = tempcnt.EndDate, DateDiff = diff1, TempContentId = tempcnt.Id, Price = SemPrice });
                                        }

                                        break;

                                    case 3:
                                        var chapSearch = repo.ChapterRepository.FindMany(s => s.Id == tempcnt.ContentId).ToList();

                                        foreach (var st in chapSearch)
                                        {
                                            var lectCount = repo.LectureRepository.FindMany(s => s.ChapterId == st.Id).ToList();
                                            int PriceMatrixChap = repo.PriceMatrixRepository.FindMany(s => s.SubscriptionTypeId == 3).Select(s => s.Price).FirstOrDefault();
                                            int chapPrice = lectCount.Count() * PriceMatrixChap;

                                            chapeDto.Add(new ChapterDto() { Id = st.Id, Title = st.Title, CreatedDate = tempcnt.StartDate, EndDate = tempcnt.EndDate, DateDiff = diff1, TempContentId = tempcnt.Id, Price = chapPrice });

                                            var lecture = repo.LectureRepository.FindMany(s => s.ChapterId == st.Id).ToList();
                                            foreach (var lt in lecture)
                                            {
                                                lectDto.Add(new LectureDto { LectureTitle = lt.LectureTitle, VideoURL = lt.VideoURL });
                                            }
                                        }

                                        break;

                                    case 2:
                                        var subSearch = repo.SubjectRepository.FindMany(s => s.Id == tempcnt.ContentId).ToList();
                                        int PriceMatrixSubject = repo.PriceMatrixRepository.FindMany(s => s.SubscriptionTypeId == 2).Select(s => s.Price).FirstOrDefault();

                                        foreach (var st in subSearch)
                                        {
                                            subjDto.Add(new SubjectDto() { Id = st.Id, SubjectTitle = st.SubjectTitle, CreatedDate = tempcnt.StartDate, EndDate = tempcnt.EndDate, DateDiff = diff1, TempContentId = tempcnt.Id, Price = PriceMatrixSubject });
                                        }

                                        break;

                                }
                            }

                        }

                    }
                    dashdto.ContentId = contId;
                    dashdto.SDto = semDto;
                    dashdto.chapDto = chapeDto;
                    dashdto.lectDto = lectDto;
                    dashdto.subDto = subjDto;

                }
                cntList.Add(dashdto);
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
     
            
            return cntList;
        }

        public void InsertDataIntoFinalTbl(int id, string SessionId, int total)
        {
            try
            {
                using (var repo = new EfUnitOfWork())
                {

                    var UsrSubsData = repo.Temp_UserSubscriptionRepository.FindMany(s => s.Sessionid == SessionId && s.UserId == id && s.Isprocess == false && s.IsRemoved == false).ToList();
                    foreach (var item1 in UsrSubsData)
                    {
                        var UsrSubslst = new UserSubcription
                        {
                            UserId = item1.UserId,
                            SubscriptionTypeId = item1.SubscriptionTypeId,
                            CreateDate = item1.CreateDate,
                            UpdateDate = item1.UpdateDate,
                            //Sessionid = SessionId,
                            IsRemoved = item1.IsRemoved

                        };

                        repo.UsersubRepository.InsertOrUpdate(UsrSubslst);

                        item1.Isprocess = true;
                        item1.IsRemoved = true;
                        repo.Temp_UserSubscriptionRepository.InsertOrUpdate(item1);
                        repo.Commit();

                        var userSubscriptionId = UsrSubslst.Id;

                        var Subsdata = repo.Temp_SubscriptionDetailsRepository.FindMany(s => s.UserId == id && s.Isprocess == false && s.IsRemoved == false);
                        foreach (var item in Subsdata)
                        {
                            var SubsDtllst = new SubscriptionDetails
                            {
                                //Id = item.Id,
                                UserId = item.UserId,
                                SubscriptionTypeId = item.SubscriptionTypeId,
                                //UserSubscriptionId = itemUsrData.Id,  // item1.Id,
                                UserSubscriptionId = userSubscriptionId,  // item1.Id,
                                ContentId = item.ContentId,
                                StartTime = item.StartTime,
                                EndTime = item.EndTime,
                                CreateDate = item.CreateDate,
                                UpdateDate = item.UpdateDate,
                                IsRemoved = item.IsRemoved
                            };

                            repo.SubdtlRepository.InsertOrUpdate(SubsDtllst);
                            //repo.Commit();

                            item.Isprocess = true;
                            item.IsRemoved = true;
                            repo.Temp_SubscriptionDetailsRepository.InsertOrUpdate(item);
                            repo.Commit();

                            var subscriptionDetailsId = SubsDtllst.Id;


                            var cntDtldata = repo.Temp_ContentDetailsRepository.FindMany(s => s.TempSubscriptionDetailId == item.Id && s.Isprocess == false && s.IsRemoved == false);
                            foreach (var item2 in cntDtldata)
                            {
                                var Cntdtllst = new ContentDetails
                                {
                                    ContentId = item2.ContentId,
                                    //SubscriptionDetailId = itemcmtData.Id,
                                    SubscriptionDetailId = subscriptionDetailsId,
                                    StartDate = item2.StartDate,
                                    EndDate = item2.EndDate,
                                    CreateDate = item2.CreateDate,
                                    UpdateDate = item2.UpdateDate,
                                    IsRemoved = item2.IsRemoved

                                };
                                repo.ContentdtlRepository.InsertOrUpdate(Cntdtllst);
                                //repo.Commit();

                                item2.Isprocess = true;
                                item2.IsRemoved = true;
                                repo.Temp_ContentDetailsRepository.InsertOrUpdate(item2);
                                repo.Commit();
                            }
                        }
                        orderService.InsertOrderDetails(id, total, userSubscriptionId);
                    }
                    repo.Dispose();
                }
            }
                catch (Exception ex)
                 {
                    // code for exception handling
                 }
           
     
        }

        public List<string> getu(int uid)
        {
            System.Collections.Generic.List<string> subType = new List<string>();
            try
            {             

                using (var repo = new EfUnitOfWork())
                {

                    var sid = repo.Temp_UserSubscriptionRepository.FindMany(s => s.UserId == uid).ToList();
                    foreach (var item in sid)
                    {
                        //int suid = Convert.ToInt16(item.SubscriptionTypeId);
                        var res = repo.SubRepository.FindMany(s => s.Id == item.SubscriptionTypeId).ToList();

                        foreach (var item1 in res)
                        {
                            subType.Add(item1.SubscriptionTypeTitle);
                        }

                    }
                    //return subType;
                }

            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
            return subType;
        }
    }
}
