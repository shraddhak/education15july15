﻿using Education.DataAccess;
using Education.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Service
{
   public class ProfessorProfileService
    {

       public List<ProfessorDto> GetData()
       {
           List<ProfessorDto> objProfessorDto = new List<ProfessorDto>();
           try
           {
              
               using (var repo = new EfUnitOfWork())
               {
                   var result = repo.ProfessorDetailsRepository.FindMany(f => f.IsRemoved == false);
                   foreach (var data in result)
                   {
                       ProfessorDto obj = new ProfessorDto();
                       obj.ProfessorName = data.ProfessorName;
                       obj.ProfessorImg = data.ProfessorImg;
                       obj.Profile = data.Profile;
                       obj.DetailsDescription = data.DetailsDescription;
                       objProfessorDto.Add(obj);

                   }

               }
              
           }
           catch (Exception ex)
           {
               //Code for exception handling
           }
           return objProfessorDto;   
         
       }








    }
}
