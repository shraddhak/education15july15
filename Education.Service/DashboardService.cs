﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Education.Dtos;
using Education.DataAccess;
using Education.Master;

namespace Education.Service
{
    public class DashboardService
    {

        public List<DashboardDto> GetListByTempCntId(int uid)
        {
            List<DashboardDto> cntList = new List<DashboardDto>();
            try
            {
              
                DashboardDto dashdto = new DashboardDto();
                List<SemesterDto> semDto = new List<SemesterDto>();
                List<ChapterDto> chapeDto = new List<ChapterDto>();
                List<SubjectDto> subjDto = new List<SubjectDto>();
                List<LectureDto> lectDto = new List<LectureDto>();
                int contId = 0;

                using (var repo = new EfUnitOfWork())
                {
                    var SubscriptionTypeId = repo.UsersubRepository.FindMany(s => s.UserId == uid).Where(s => s.IsRemoved == false).ToList();
                    foreach (var sid in SubscriptionTypeId)
                    {
                        var SubscriptionTypeTitle = repo.SubRepository.FindMany(s => s.Id == sid.SubscriptionTypeId).Select(s => s.SubscriptionTypeTitle).ToList();
                        foreach (string a in SubscriptionTypeTitle) { dashdto.SubscriptionTypeTitle = a; }
                        var TempUsrSubid = repo.SubdtlRepository.FindMany(s => s.UserSubscriptionId == sid.Id).Where(s => s.IsRemoved == false).ToList();
                        foreach (var Tusrid in TempUsrSubid)
                        {
                            contId = Tusrid.ContentId;
                            var TempCntDtls = repo.ContentdtlRepository.FindMany(s => s.SubscriptionDetailId == Tusrid.Id).Where(s => s.IsRemoved == false).ToList();

                            foreach (var tempcnt in TempCntDtls)
                            {
                                DateTime sDate = DateTime.Now; //Convert.ToDateTime(tempcnt.StartDate);
                                DateTime eDate = Convert.ToDateTime(tempcnt.EndDate);

                                string diff1;
                                TimeSpan span = eDate.Subtract(sDate);
                                string hour = span.Hours.ToString();
                                string days = span.Days.ToString();

                                // if (!(days == 0 && hour == 0))  //
                                // {
                                diff1 = "Days: " + span.Days.ToString() + " Hours: " + span.Hours.ToString();                //  TotalDays.ToString() + " Days ";    // +TotalHours.ToString() + " Hours";
                                //} 
                                //else
                                // { 
                                //     diff1 = "-"; 
                                //}

                                switch (contId)
                                {
                                    case 1:
                                        var semSearch = repo.SemesterRepository.FindMany(s => s.Id == tempcnt.ContentId).ToList();
                                        var subCnt = repo.SubjectRepository.FindMany(s => s.SemesterId == tempcnt.ContentId).ToList();
                                        int PriceMatrix = repo.PriceMatrixRepository.FindMany(s => s.SubscriptionTypeId == 1).Select(s => s.Price).FirstOrDefault();
                                        int SemPrice = subCnt.Count() * PriceMatrix;
                                        foreach (var st in semSearch)
                                        {

                                            semDto.Add(new SemesterDto() { Id = st.Id, SemesterTitle = st.SemesterTitle, CreatedDate = tempcnt.StartDate, EndDate = tempcnt.EndDate, DateDiff = diff1, TempContentId = tempcnt.Id, Price = SemPrice });
                                        }

                                        break;

                                    case 3:
                                        var chapSearch = repo.ChapterRepository.FindMany(s => s.Id == tempcnt.ContentId).ToList();

                                        foreach (var st in chapSearch)
                                        {
                                            var lectCount = repo.LectureRepository.FindMany(s => s.ChapterId == st.Id).ToList();
                                            int PriceMatrixChap = repo.PriceMatrixRepository.FindMany(s => s.SubscriptionTypeId == 3).Select(s => s.Price).FirstOrDefault();
                                            int chapPrice = lectCount.Count() * PriceMatrixChap;

                                            chapeDto.Add(new ChapterDto() { Id = st.Id, Title = st.Title, CreatedDate = tempcnt.StartDate, EndDate = tempcnt.EndDate, DateDiff = diff1, TempContentId = tempcnt.Id, Price = chapPrice });

                                            var lecture = repo.LectureRepository.FindMany(s => s.ChapterId == st.Id).ToList();
                                            foreach (var lt in lecture)
                                            {
                                                lectDto.Add(new LectureDto { LectureTitle = lt.LectureTitle, VideoURL = lt.VideoURL });
                                            }
                                        }

                                        break;

                                    case 2:
                                        var subSearch = repo.SubjectRepository.FindMany(s => s.Id == tempcnt.ContentId).ToList();
                                        int PriceMatrixSubject = repo.PriceMatrixRepository.FindMany(s => s.SubscriptionTypeId == 2).Select(s => s.Price).FirstOrDefault();

                                        foreach (var st in subSearch)
                                        {
                                            subjDto.Add(new SubjectDto() { Id = st.Id, SubjectTitle = st.SubjectTitle, CreatedDate = tempcnt.StartDate, EndDate = tempcnt.EndDate, DateDiff = diff1, TempContentId = tempcnt.Id, Price = PriceMatrixSubject });
                                        }

                                        break;

                                }
                            }

                        }

                    }
                    dashdto.ContentId = contId;
                    dashdto.SDto = semDto;
                    dashdto.chapDto = chapeDto;
                    dashdto.lectDto = lectDto;
                    dashdto.subDto = subjDto;

                }
                cntList.Add(dashdto);
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
           
           
            return cntList;
        }
    }

}
