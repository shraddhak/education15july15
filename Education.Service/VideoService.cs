﻿using Education.DataAccess;
using Education.Dtos;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Service
{
  public  class VideoService
    {

      //Professor Details
      public LectureDto GetLectureDetails(int Id)
        {
            using (var repo = new EfUnitOfWork())
            {
                var lst = new List<LectureDto>();
                var data = repo.LectureRepository.FindMany(s => s.Id == Id).FirstOrDefault();

                LectureDto lectDto = new LectureDto();
                lectDto.LectureContent = data.LectureContent;
            
                repo.Commit();
                repo.Dispose();
                return lectDto;
            }
        }


      public List<LectureDto> GetAllLecture()
      {
          using (var repo = new EfUnitOfWork())
          {
              var lst = new List<LectureDto>();
              var data = repo.LectureRepository.All;

              foreach (var item in data)
              {
                  lst.Add(new LectureDto
                  {
                      Id = item.Id,
                      LectureTitle = item.LectureTitle
                  });
              }

       

              repo.Commit();
              repo.Dispose();
              return lst;
          }
      }
      
    }




}
