﻿using Education.DataAccess;
using Education.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.SessionState;

namespace Education.Service
{
   public class SubscribeService
    {
       

       public SubscriptionDetailDto InsertData(SubscriptionDetailDto Objsubscription)
       {
           try
           {
               var Request = HttpContext.Current.Request;

               //Insert data After validation
               var service = new UserService();
               var tusub = new Temp_UserSubscriptionDto();
               var subd = new Temp_SubscriptionDetailsDto();
               var tcont = new Temp_ContentDetailsDto();

               int userid = Objsubscription.UserId;
               int subscriptiotypeid = Objsubscription.Subscriptions;

               string[] subid;

               if (subscriptiotypeid == 2)
               {
                   subid = Objsubscription.Subject1;
               }
               else
               {

                   subid = Objsubscription.Subject;
               }

               string[] semid = Objsubscription.Semester;

               string[] chpid = Objsubscription.Chapter;

               int subid1 = Objsubscription.Subscriptions;

               string userEmail = service.GetEmailByUsrId(Objsubscription.UserId);

               string subsTitle = string.Empty;
               string chapsTitle = string.Empty;
               string semsTitle = string.Empty;

               DateTime starttime = DateTime.Now;

               DateTime endtime = starttime.AddDays(120);

               //set cookie

               string UserSessionID = HttpContext.Current.Session["UserSessionId"].ToString();

               HttpCookie UserCookieID = new HttpCookie("UserCookieId", UserSessionID);
               UserCookieID.Expires.AddDays(180);

               if (Objsubscription.IsvalidateFlag == false)
               {
                   switch (subid1)
                   {
                       case 1:

                           tusub.UserId = userid;
                           tusub.SubscriptionTypeId = subid1;
                           tusub.Sessionid = UserSessionID;
                           int usersubid = service.AddTempUserSubscription(tusub);

                           subd.UserId = userid;
                           subd.SubscriptionTypeId = subid1;
                           subd.TempUserSubscriptionId = usersubid;
                           subd.ContentId = 1;
                           subd.StartTime = starttime.ToString();
                           subd.EndTime = starttime.ToString();
                           int subdid = service.AddTempSubscriptionDetails(subd);

                           foreach (var semobj in semid)
                           {

                               string semname = this.GetSemNameById(Convert.ToInt16(semobj));
                               semsTitle = semsTitle + "," + semname;
                               tcont.ContentId = Convert.ToInt16(semobj);
                               tcont.TempSubscriptionDetailId = subdid;
                               tcont.StartDate = starttime.ToString();
                               tcont.EndDate = endtime.ToString();
                               service.AddTempContentDeatils(tcont);
                           }

                           //set cookie

                           // HttpCookie UserCookieID = new HttpCookie("UserCookieId", UserSessionID);                       
                           HttpContext.Current.Response.Cookies.Add(UserCookieID);
                           SendMail(semsTitle, userEmail);
                           break;

                       case 2:
                           //Insert into UserSubscription 
                           tusub.UserId = userid;
                           tusub.SubscriptionTypeId = subid1;
                           tusub.Sessionid = UserSessionID;
                           int usersubid1 = service.AddTempUserSubscription(tusub);

                           //Insert into SubscriptionDetails
                           subd.UserId = userid;
                           subd.SubscriptionTypeId = subid1;
                           subd.TempUserSubscriptionId = usersubid1;
                           subd.ContentId = 2;
                           subd.StartTime = starttime.ToString();
                           subd.EndTime = starttime.ToString();
                           int subdid1 = service.AddTempSubscriptionDetails(subd);

                           //Insert into ContentDetails
                           foreach (var subject in subid)
                           {
                               string subname = this.GetSubNameById(Convert.ToInt16(subject));
                               subsTitle = subsTitle + "," + subname;
                               tcont.ContentId = Convert.ToInt16(subject);
                               tcont.TempSubscriptionDetailId = subdid1;
                               tcont.StartDate = starttime.ToString();
                               tcont.EndDate = endtime.ToString();

                               service.AddTempContentDeatils(tcont);

                           }
                           // HttpCookie UserCookieIDSub = new HttpCookie("UserCookieId", UserSessionID);
                           HttpContext.Current.Response.Cookies.Add(UserCookieID);
                           SendMail(subsTitle, userEmail);
                           break;

                       case 3:

                           //Insert into UserSubscription 
                           tusub.UserId = userid;
                           tusub.SubscriptionTypeId = subid1;
                           tusub.Sessionid = UserSessionID;
                           int usersubid2 = service.AddTempUserSubscription(tusub);

                           //Insert into SubscriptionDetails
                           subd.UserId = userid;
                           subd.SubscriptionTypeId = subid1;
                           subd.TempUserSubscriptionId = usersubid2;
                           subd.ContentId = 3;
                           subd.StartTime = starttime.ToString();
                           subd.EndTime = starttime.ToString();
                           int subdid2 = service.AddTempSubscriptionDetails(subd);

                           //Insert into ContentDetails
                           foreach (var chapterobj in chpid)
                           {
                               string chaptitle = this.GetChapNameById(Convert.ToInt16(chapterobj));
                               chapsTitle = chapsTitle + "," + chaptitle;
                               tcont.ContentId = Convert.ToInt16(chapterobj);
                               tcont.TempSubscriptionDetailId = subdid2;
                               tcont.StartDate = starttime.ToString();
                               tcont.EndDate = endtime.ToString();

                               service.AddTempContentDeatils(tcont);

                           }
                           //HttpCookie UserCookieIDChap = new HttpCookie("UserCookieId", UserSessionID);
                           HttpContext.Current.Response.Cookies.Add(UserCookieID);
                           SendMail(chapsTitle, userEmail);
                           break;


                   }
               }
           }
           catch (Exception ex)
           {
               //Code for exception handling
           }
 

           return Objsubscription;


       }

       public void SendMail(string Body, string Email)
       {
           try
           {
               MailMessage mail = new MailMessage();
               mail.To.Add(Email);
               mail.From = new MailAddress("bhaveanand02@gmail.com");
               mail.Subject = "SubscribtionDetails";
               string MailBody = "You have Subscribed For " + Body + "";
               mail.Body = MailBody;
               mail.IsBodyHtml = true;
               SmtpClient smtp = new SmtpClient();

               smtp.Send(mail);
           }
           catch (Exception ex)
           {
               //Code for exception handling
           }
          

       }

       public string GetSemNameById(int id)
       {
           string SemesterName = string.Empty;
           try
           {
              
               using (var repo = new EfUnitOfWork())
               {
                   SemesterName = repo.SemesterRepository.FindMany(s => s.Id == id).Select(s => s.SemesterTitle).FirstOrDefault();

               }
           }
           catch (Exception ex)
           {
               //Code for exception handling
           }
          
           return SemesterName;
       }

       public string GetChapNameById(int id)
       {
           string ChaperName = string.Empty;
           try
           {
               using (var repo = new EfUnitOfWork())
               {
                   ChaperName = repo.ChapterRepository.FindMany(s => s.Id == id).Select(s => s.Title).FirstOrDefault();

               }
           }
           catch (Exception ex)
           {
               //Code for exception handling
           }
           
           return ChaperName;
       }

       public string GetSubNameById(int id)
       {

           string SubjectName = string.Empty;
           try
           {
               using (var repo = new EfUnitOfWork())
               {
                   SubjectName = repo.SubjectRepository.FindMany(s => s.Id == id).Select(s => s.SubjectTitle).FirstOrDefault();

               }
           }
           catch (Exception ex)
           {
               //Code for exception handling
           }
          
           return SubjectName;
       }

    }
}
