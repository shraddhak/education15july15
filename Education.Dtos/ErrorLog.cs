﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Dtos
{
  public class ErrorLog : Dto
    {
        public int Id { get; set; }
        public int ModulesId { get; set; }
        public string Page { get; set; }
        public string Stacktrace { get; set; }
        public string Message { get; set; }

    }
}
