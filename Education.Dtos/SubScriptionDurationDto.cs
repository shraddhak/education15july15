﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Dtos
{
    public class SubScriptionDurationDto: Dto
    {
        public string Title { get; set; }

        public int NoOfDays { get; set; }

        public int SubscriptionTypeId { get; set; }

        public SubscriptionTypeDto SubscriptionTypeDto { get; set; }
    }
}
