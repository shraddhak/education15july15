﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Education.Master;

namespace Education.Dtos
{
    public class SubjectDto: Dto
    {
        public string SubjectTitle { get; set; }

        public int SemesterId { get; set; }

        public Semester Semester { get; set; }

        public string CreatedDate { get; set; }

        public string EndDate { get; set; }

        public string DateDiff { get; set; }

        public int TempContentId { get; set; }

        public int Price { get; set; }
    }
}
