﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Dtos
{
    public class UserSession: Dto
    {
        public string UserSessionID { get; set; }
        public int UserId { get; set; }
        public int OTP { get; set; }
    }
}
