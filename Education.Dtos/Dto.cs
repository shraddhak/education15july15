﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Dtos
{
    public class Dto
    {
        private DateTime _createDate;


        public Dto()
        {
            // for a new entity, CreateDate and UpdateDate should be the same
            CreateDate = UpdateDate = DateTime.Now;
        }


        public int Id
        {
            get;
            set;

        }

        public DateTime CreateDate
        {
            get
            {
                return

                    _createDate == DateTime.MinValue ? DateTime.Now : _createDate;
            }


            set { _createDate = value; }
        }

        public DateTime UpdateDate
        {

            get;
            set;
        }

        public bool IsRemoved { get; set; }
 
    }
}
