﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Education.Master;

namespace Education.Dtos
{
    public class SemesterDto: Dto
    {
        public string SemesterTitle { get; set; }

        public int YearId { get; set; }

        public Year Year { get; set; }

        public string CreatedDate { get; set; }

        public string EndDate { get; set; }

        public string DateDiff { get; set; }

         public ICollection<Subject> Subject { get; set; }

         public int TempContentId { get; set; }

         public int Price { get; set; }
    }
}
