﻿using Education.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIND.Dtos
{
    public class CustomerFeedback : Dto
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Subject { get; set; }
        public string SubjectRating { get; set; }
        public string Professor { get; set; }
        public string ProfessorRating { get; set; }
        public string FeedBack { get; set; }
    }
}
