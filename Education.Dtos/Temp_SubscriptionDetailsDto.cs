﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Dtos
{
    public class Temp_SubscriptionDetailsDto: Dto
    {
        public int UserId { get; set; }
        public UserDto User { get; set; }

        public int SubscriptionTypeId { get; set; }
        public SubscriptionTypeDto SubscriptionTypeDto { get; set; }


        public int TempUserSubscriptionId { get; set; }

        public Temp_UserSubscriptionDto Temp_UsersubscriptionDto { get; set; }

        public int ContentId { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }

       public ICollection<Temp_ContentDetailsDto> Temp_ContentDetailsDto { get; set; }

    }
}
