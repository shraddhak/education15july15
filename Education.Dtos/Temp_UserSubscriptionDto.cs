﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Dtos
{
    public class Temp_UserSubscriptionDto: Dto
    {
        public int UserId { get; set; }
        
        public UserDto User { get; set; }

        public int SubscriptionTypeId { get; set; }
        
        public SubscriptionTypeDto SubscriptionTypeDto { get; set; }
        
        public string Title { get; set; }

        public string Sessionid { get; set; }

        public ICollection<Temp_SubscriptionDetailsDto> Temp_SubscriptionDeatilsDto { get; set; }
    }
}
