﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Dtos
{
   public class ContentDetailsDto: Dto
    {
        public int ContentId { get; set; }

        public int SubscriptionDetailId { get; set; }
        public SubscriptionDetailDto SubscriptionDetails { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }
    }
}
