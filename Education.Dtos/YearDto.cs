﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Education.Master;

namespace Education.Dtos
{
    public class YearDto: Dto
    {
        public string YearTitle { get; set; }

        public int BranchId { get; set; }

        public Branch Branch { get; set; }

        public ICollection<Semester> Semester { get; set; }
    }
}
