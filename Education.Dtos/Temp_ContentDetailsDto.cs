﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Dtos
{
   public class Temp_ContentDetailsDto: Dto
    {
       public int ContentId { get; set; }

       public string StartDate { get; set; }

       public string EndDate { get; set; }

       public int TempSubscriptionDetailId { get; set; }
       public Temp_ContentDetailsDto Temp_SubscriptionDetailsDto { get; set; }

    }
}
