﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Dtos
{
    public class SubDetailsDto
    {
        public List<Sem> Semesters { get; set; }
    }

    public class Sem
    {
        public string SemTitle { get; set; }
        public List<Sub> Subjects { get; set; }
    }

    public class Sub
    {
        public string SubTitle { get; set; }
        public List<Chap> Chapters { get; set; }
    }

    public class Chap
    {
        public string ChapTitle { get; set; }
        public List<Lecture> Lectures { get; set; }
    }

    public class Lecture
    {
        public string LectTitle { get; set; }
    }
}
