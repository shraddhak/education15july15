﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Dtos
{
    public class ChapterDto: Dto
    {
        public SubjectDto Subject { get; set; }

        public int SubjectId { get; set; }

        public string Title { get; set; }

        public string CreatedDate { get; set; }

        public string EndDate { get; set; }

        public string DateDiff { get; set; }

        public ICollection<LectureDto> Lecture { get; set; }

        public int TempContentId { get; set; }

        public int Price { get; set; }
    }
}
