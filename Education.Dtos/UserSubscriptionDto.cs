﻿using Education.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Dtos
{
    public class UserSubscriptionDto: Dto
    {
        public int UserId { get; set; }
        public User User { get; set; }

        public int SubscriptionTypeId { get; set; }
        public string Sessionid { get; set; }
        public ICollection<SubscriptionDetails> SubscriptionDetails { get; set; }
        public string Title { get; set; }
    }
}
