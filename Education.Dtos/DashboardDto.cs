﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Dtos
{
    public class DashboardDto:Dto
    {
        public int SubscriptionTypeId { get; set; }
        public string SubscriptionTypeTitle { get; set; }
        public int TempContentDetailsId { get; set; }
        public int ContentId { get; set; }
        public int TempSubscribtionDetailsId { get; set; }
        public int UserId { get; set; }
        public string TempUserSubscribtionId { get; set; }
        public List<string> SemesterTitle { get; set; }
        public List<SemesterDto> SDto { get; set; }
        public List<string> SubjectTitle { get; set; }
        public List<SubjectDto> subDto { get; set; }
        public List<string> ChapterTitle { get; set; }
        public List<ChapterDto> chapDto { get; set; }
        public List<LectureDto> lectDto { get; set; }
    }
}
