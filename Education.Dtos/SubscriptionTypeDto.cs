﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Dtos
{
    public class SubscriptionTypeDto : Dto
    {
        public string SubscriptionTypeTitle { get; set; }
    }
}
