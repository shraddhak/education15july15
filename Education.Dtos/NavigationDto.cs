﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIND.Dtos
{
    public class NavigationDto
    {
        public int SemesterId { get; set; }
        public string SemesterName { get; set; }
        public List<SubjectDto> subDto { get; set; }        
        public List<ChapterDto> chapDto { get; set; }
        public List<LectureDto> lectDto { get; set; }
    }
}
