﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Dtos
{
    public class LectureDto: Dto
    {
        public string LectureTitle { get; set; }

        public int ChapterId { get; set; }
        public ChapterDto Chapter { get; set; }


        public string VideoURL { get; set; }

        public int ProfessorId { get; set; }

        public string LectureContent { get; set; }

    }
}
