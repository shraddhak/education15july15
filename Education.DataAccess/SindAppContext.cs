﻿using Education.Master;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Education.DataAccess
{
    public class EducationAppContext : DbContext
    {

        public EducationAppContext()
            : base("DefaultConnection")
        {
            Database.SetInitializer<EducationAppContext>(null);
        }

        public DbSet<Subject> Subject { get; set; }

        public DbSet<Chapter> Chapter { get; set; }

        public DbSet<Video> Video { get; set; }

        public DbSet<Test> Tests { get; set; }

        public DbSet<Branch> Branch { get; set; }

        public DbSet<Year> Year { get; set; }

        public DbSet<Semester> Semester { get; set; }

        public DbSet<Lecture> Lecture { get; set; }

        public DbSet<User> User { get; set; }

        public DbSet<SubscriptionType> SubscriptionType { get; set; }

        public DbSet<UserSubcription> UserSubcription { get; set; }

        public DbSet<SubscriptionDetails> SubscriptionDetails { get; set; }

        public DbSet<ContentDetails> ContentDetails { get; set; }

        public DbSet<Temp_UserSubscription> Temp_UserSubscription { get; set; }

        public DbSet<Temp_SubscriptionDetails> Temp_SubscriptionDetails { get; set; }

        public DbSet<Temp_ContentDetails> Temp_ContentDetails { get; set; }

        public DbSet<ProfessorDetails> ProfessorDetails { get; set; }

        public DbSet<PriceMatrix> PriceMatrix { get; set; }

        public DbSet<SubscriptionDuration> SubscriptionDuration { get; set; }

        public DbSet<Orders> Orders { get; set; }

        public DbSet<Order_Details> OrderDetails { get; set; }

        public DbSet<UserSession> UserSession { get; set; }

        public DbSet<Modules> Modules { get; set; }

        public DbSet<ErrorLog> ErrorLog { get; set; }

        public DbSet<CustomerFeedback> CustomerFeedback { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        //public System.Data.Entity.DbSet<Education.Dtos.UserDto> UserDtoes { get; set; }

        //public System.Data.Entity.DbSet<Education.Dtos.UserDto> UserDtoes { get; set; }

       
  }


}
