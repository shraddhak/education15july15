﻿using Education.Master;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;


namespace Education.DataAccess
{
    public class Repository
    {

    }
    public class Repository<TEntity> : IRepository<TEntity>
       where TEntity : Entity
    {
        private readonly DbSet<TEntity> _dbSet;

        public Repository(EducationAppContext context)
        {
            var EducationAppContext = context as EducationAppContext;
            if (EducationAppContext == null) throw new Exception("Must be a valid context"); // TODO: Typed exception
            _dbSet = context.Set<TEntity>();
        }

        public IEnumerable<TEntity> All
        {
            get { return _dbSet; }
        }

        public IEnumerable<TEntity> AllIncluding(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return includeProperties.Aggregate<Expression<Func<TEntity, object>>, IQueryable<TEntity>>(_dbSet, (current, includeProperty) => current.Include(includeProperty));
        }

        public TEntity FindFirstOrDefault(Expression<Func<TEntity, bool>> where)
        {
            return _dbSet.FirstOrDefault(@where);
        }

        public IEnumerable<TEntity> FindMany(Func<TEntity, bool> where)
        {
            return _dbSet.Where(@where).ToList();
        }

        public IEnumerable<TEntity> FindMany(Expression<Func<TEntity, bool>> where, Func<TEntity, object> orderBy, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var entities = _dbSet.Where(@where);
            entities = includeProperties.Aggregate(entities, (current, includeProperty) => current.Include(includeProperty));
            return entities.OrderBy(orderBy).ToList();
        }

        public IEnumerable<TEntity> FindMany(Expression<Func<TEntity, bool>> where, Func<TEntity, object> orderBy, int page, int pageSize, out int rows)
        {
            page = page - 1;
            rows = _dbSet.Count(@where);

            return _dbSet.Where(@where)
                         .OrderBy(orderBy)
                         .Skip(page * pageSize)
                         .Take(pageSize)
                         .ToList();
        }

        public IEnumerable<TEntity> FindMany(Expression<Func<TEntity, bool>> where, Func<TEntity, object> orderBy, int page, int pageSize, out int rows,
            params Expression<Func<TEntity, object>>[] includeProperties)
        {
            page = page - 1;
            rows = _dbSet.Count(@where);

            return AllIncluding(includeProperties).ToList()
                                                  .Where(@where.Compile())
                                                  .OrderBy(orderBy)
                                                  .Skip(page * pageSize)
                                                  .Take(pageSize)
                                                  .ToList();
        }

        public TEntity Find(int id)
        {
            return _dbSet.Find(id);
        }

        public TEntity Find(int id, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return AllIncluding(includeProperties).FirstOrDefault(e => e.Id == id);
        }

        public void InsertOrUpdate(TEntity T)
        {
            if (T.Id == default(int))
            {

            }
            else
            {

                T.UpdateDate = DateTime.Now;
            }
            _dbSet.AddOrUpdate(T);
        }

        public void Delete(int id)
        {
            TEntity entity = Find(id);
            _dbSet.Remove(entity);
        }
    }
}
