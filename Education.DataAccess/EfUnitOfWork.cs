﻿
using Education.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity.Validation;
using System.Diagnostics;

namespace Education.DataAccess
{
   
       public class EfUnitOfWork : IUnitOfWork, IDisposable
       {
           private EducationAppContext context = new EducationAppContext();

           private Repository<Subject> subjectRepository { get; set; }

           private Repository<Chapter> chapterRepository { get; set; }

           private Repository<Video> videoRepository { get; set; }

           private Repository<Branch> branchRepository { get; set; }

           private Repository<Year> yearRepository { get; set; }

           private Repository<Semester> semesterRepository { get; set; }

           private Repository<Lecture> lectureRepository { get; set; }

           private Repository<User> userRepository { get; set; }

           private Repository<SubscriptionType> subRepository { get; set; }

           private Repository<UserSubcription> usersubRepository { get; set; }

           private Repository<SubscriptionDetails> subdtlRepository { get; set; }

           private Repository<ContentDetails> contentdtlRepository { get; set; }

           private Repository<Temp_UserSubscription> tempuserSubscriptionRepository { get; set; }

           private Repository<Temp_SubscriptionDetails> tempsubscriptionDetailsRepository { get; set; }

           private Repository<Temp_ContentDetails> tempcontentDetailsRepository { get;set; }

           private Repository<ProfessorDetails> professorDetailsRepository { get; set; }

           private Repository<PriceMatrix> priceMatrixRepository { get; set; }

           private Repository<SubscriptionDuration> subscriptionDurationRepository { get; set; }

           private Repository<Orders> ordersRepository { get; set; }

           private Repository<Order_Details> orderDetailsRepository { get; set; }

           private Repository<UserSession> userSessionRepository { get; set; }

           private Repository<Modules> moduleRepository { get; set; }

           private Repository<ErrorLog> errorLogRepopsitory { get; set; }

           private Repository<CustomerFeedback> customerFeedbackRepository { get; set; }
          

           public EfUnitOfWork()
           {

           }


           public Repository<Subject> SubjectRepository
           {
               get
               {
                   if (this.subjectRepository == null)
                   {
                       this.subjectRepository = new Repository<Subject>(context);
                   }
                   return subjectRepository;
               }

           }

           public Repository<Chapter> ChapterRepository
           {
               get
               {
                   if (this.chapterRepository == null)
                   {
                       this.chapterRepository = new Repository<Chapter>(context);
                   }
                   return chapterRepository;
               }

           }

           public Repository<Video> VideoRepository
           {
               get
               {
                   if (this.videoRepository == null)
                   {
                       this.videoRepository = new Repository<Video>(context);
                   }
                   return videoRepository;
               }

           }

           public Repository<Branch> BranchRepository
           {
               get
               {
                   if (this.branchRepository == null)
                   {
                       this.branchRepository = new Repository<Branch>(context);
                   }
                   return branchRepository;
               }

           }

           public Repository<Year> YearRepository
           {
               get
               {
                   if (this.yearRepository == null)
                   {
                       this.yearRepository = new Repository<Year>(context);
                   }
                   return yearRepository;
               }

           }

           public Repository<Semester> SemesterRepository
           {

               get 
               {
               
                if(this.semesterRepository==null)
                {

                    this.semesterRepository = new Repository<Semester>(context);
                }

                return semesterRepository;
               
               }
           }

           public Repository<Lecture> LectureRepository
           {

               get
               {

                   if (this.lectureRepository == null)
                   {

                       this.lectureRepository = new Repository<Lecture>(context);
                   }

                   return lectureRepository;

               }
           }

           public Repository<User> UserRepository
           {

               get
               {

                   if (this.userRepository == null)
                   {

                       this.userRepository = new Repository<User>(context);
                   }

                   return userRepository;

               }
           }


           public Repository<SubscriptionType> SubRepository
           {

               get
               {

                   if (this.subRepository == null)
                   {

                       this.subRepository = new Repository<SubscriptionType>(context);
                   }

                   return subRepository;

               }
           }

           public Repository<UserSubcription> UsersubRepository
           {

               get
               {

                   if (this.usersubRepository == null)
                   {

                       this.usersubRepository = new Repository<UserSubcription>(context);
                   }

                   return usersubRepository;

               }
           }


           public Repository<SubscriptionDetails> SubdtlRepository
           {
               get
               {
                   if(this.subdtlRepository==null)
                   {

                       this.subdtlRepository = new Repository<SubscriptionDetails>(context);

                   }

                   return this.subdtlRepository;
               }
           }

           public Repository<ContentDetails> ContentdtlRepository
           {
               get
               {
                   if (this.contentdtlRepository == null)
                   {

                       this.contentdtlRepository = new Repository<ContentDetails>(context);

                   }

                   return this.contentdtlRepository;
               }
           }

           public Repository<Temp_UserSubscription> Temp_UserSubscriptionRepository
           {
               get
               {
                   if (this.tempuserSubscriptionRepository == null)
                   {

                       this.tempuserSubscriptionRepository = new Repository<Temp_UserSubscription>(context);

                   }

                   return this.tempuserSubscriptionRepository;
               }


           }

           public Repository<Temp_SubscriptionDetails> Temp_SubscriptionDetailsRepository
           {
               get
               {
                   if (this.tempsubscriptionDetailsRepository == null)
                   {

                       this.tempsubscriptionDetailsRepository = new Repository<Temp_SubscriptionDetails>(context);

                   }

                   return this.tempsubscriptionDetailsRepository;
               }
           }

           public Repository<Temp_ContentDetails> Temp_ContentDetailsRepository
           {
               get
               {
                   if (this.tempcontentDetailsRepository == null)
                   {

                       this.tempcontentDetailsRepository = new Repository<Temp_ContentDetails>(context);

                   }

                   return this.tempcontentDetailsRepository;
               }
           }

         

           public Repository<ProfessorDetails> ProfessorDetailsRepository
           {
               get
               {
                   if (this.professorDetailsRepository == null)
                   {

                       this.professorDetailsRepository = new Repository<ProfessorDetails>(context);

                   }

                   return this.professorDetailsRepository;
               }
           }


           public Repository<PriceMatrix> PriceMatrixRepository
           {
               get
               {
                   if (this.priceMatrixRepository == null)
                   {

                       this.priceMatrixRepository = new Repository<PriceMatrix>(context);

                   }

                   return this.priceMatrixRepository;
               }
           }

           public Repository<SubscriptionDuration> SubscriptionDurationRepository
           {
               get
               {
                   if (this.subscriptionDurationRepository == null)
                   {

                       this.subscriptionDurationRepository = new Repository<SubscriptionDuration>(context);

                   }

                   return this.subscriptionDurationRepository;
               }
           }


           public Repository<Orders> OrderRepository
           {
               get
               {
                   if (this.ordersRepository  == null)
                   {

                       this.ordersRepository = new Repository<Orders>(context);

                   }

                   return this.ordersRepository;
               }
           }


           public Repository<Order_Details> OrderDetailsRepository
           {
               get
               {
                   if (this.orderDetailsRepository== null)
                   {

                       this.orderDetailsRepository = new Repository<Order_Details>(context);

                   }

                   return this.orderDetailsRepository;
               }
           }

           public Repository<UserSession> UserSessionRepository
           {
               get
               {
                   if (this.userSessionRepository == null)
                   {

                       this.userSessionRepository = new Repository<UserSession>(context);

                   }

                   return this.userSessionRepository;
               }
           }


           public Repository<Modules> ModulesRepository
           {
               get
               {
                   if (this.moduleRepository == null)
                   {
                       this.moduleRepository = new Repository<Modules>(context);
                   }

                   return this.moduleRepository;
               }
           }

           public Repository<ErrorLog> ErrorLogRepository
           {
               get
               {
                   if (this.errorLogRepopsitory == null)
                   {
                       this.errorLogRepopsitory = new Repository<ErrorLog>(context);
                   }
                   return this.errorLogRepopsitory;
               }
           }

           public Repository<CustomerFeedback> CustomerFeedbackRepository
           {
               get
               {
                   if (this.customerFeedbackRepository == null)
                   {
                       this.customerFeedbackRepository = new Repository<CustomerFeedback>(context);
                   }
                   return this.customerFeedbackRepository;
               }
           }

           public void Commit()
           {
               try
               {
                   context.SaveChanges();
               }
               catch (DbEntityValidationException dbEx)
               {
                   foreach (var validationErrors in dbEx.EntityValidationErrors)
                   {
                       foreach (var validationError in validationErrors.ValidationErrors)
                       {
                           Debug.Write(validationError.PropertyName + ":" + validationError.ErrorMessage);
                       }
                   }
                   throw dbEx;
               }
               catch (Exception ex)
               {
                   throw;
               }
           }

           public void Dispose()
           {
               context.Dispose();
               GC.SuppressFinalize(this);
           }

       }

    }

