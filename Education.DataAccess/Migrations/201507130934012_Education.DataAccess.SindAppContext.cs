namespace SIND.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EducationDataAccessSindAppContext : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Lectures", "LectureContent", c => c.String());
            AddColumn("dbo.ProfessorDetails", "Biodata", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProfessorDetails", "Biodata");
            DropColumn("dbo.Lectures", "LectureContent");
        }
    }
}
