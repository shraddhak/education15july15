namespace SIND.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EducationDataAccessSindAppContext : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Temp_SubscriptionDetails", "Isprocess", c => c.Boolean(nullable: false));
            AddColumn("dbo.Temp_ContentDetails", "Isprocess", c => c.Boolean(nullable: false));
            AddColumn("dbo.Temp_UserSubscription", "Isprocess", c => c.Boolean(nullable: false));
            DropColumn("dbo.UserSubcriptions", "Sessionid");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserSubcriptions", "Sessionid", c => c.String());
            DropColumn("dbo.Temp_UserSubscription", "Isprocess");
            DropColumn("dbo.Temp_ContentDetails", "Isprocess");
            DropColumn("dbo.Temp_SubscriptionDetails", "Isprocess");
        }
    }
}
