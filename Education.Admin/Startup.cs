﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Education.Admin.Startup))]
namespace Education.Admin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
