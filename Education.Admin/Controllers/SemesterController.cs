﻿using Education.Admin.Filters;
using Education.Dtos;
using Education.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Education.Admin.Controllers
{
    [AuthActionFilter]
    public class SemesterController : Controller
    {
        //
        // GET: /Semester/
       
            //
            // GET: /Semester/
            public ActionResult Index()
            {
                var service = new AdminService();
                var res = service.GetSemester();
                return View(res);
            }

            [HttpGet]
            public ActionResult AddSemester()
            {
                var service = new UserService();
                var brc = service.GetYear();
                SelectList brlist = new SelectList(brc, "Id", "YearTitle");
                ViewBag.Year = brlist;
                return View();
            }

            [HttpPost]
            public ActionResult AddSemester(FormCollection form)
            {
                var semester = form["SemesterTitle"].ToString();
                int yearId = Convert.ToInt32(form["Year"]);
                var service = new AdminService();
                service.AddSemester(new SemesterDto { SemesterTitle = semester, YearId = yearId });
                return RedirectToAction("Index", "Semester");
            }

            [HttpGet]
            public ActionResult UpdateSemester(int id)
            {
                var service = new AdminService();
                var res = service.GetSemesterById(id);
                var userSer = new UserService();
                var brc = userSer.GetYear();
                SelectList brlist = new SelectList(brc, "Id", "YearTitle", res.YearId);
                ViewBag.Year = brlist;
                return View(res);
            }

            [HttpPost]
            public ActionResult UpdateSemester(SemesterDto semester, FormCollection form)
            {
                var service = new AdminService();
                semester.YearId = Convert.ToInt32(form["Year"]);
                service.UpdateSemester(semester);
                return RedirectToAction("Index", "Semester");
            }

            public ActionResult DeleteSemester(int id)
            {
                var service = new AdminService();
                service.DeleteSemester(id);
                return RedirectToAction("Index", "Semester");
            }
     
	}
}