﻿using Education.Admin.Filters;
using Education.Dtos;
using Education.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Education.Admin.Controllers
{
    [AuthActionFilter]
    public class PriceMatrixController : Controller
    {
        //
        // GET: /PriceMatrix/
       
            //
            // GET: /PriceMatrix/
            public ActionResult Index()
            {
                var service = new AdminService();
                var res = service.GetPriceMatrix();
                return View(res);//Pass res
            }

            [HttpGet]
            public ActionResult AddPriceMatrix()
            {
                var service = new AdminService();
                var brc = service.GetSubscriptionType();
                SelectList brlist = new SelectList(brc, "Id", "SubscriptionTypeTitle");
                ViewBag.SubscriptionType = brlist;
                return View();
            }

            [HttpPost]
            public ActionResult AddPriceMatrix(FormCollection form)
            {
                var price = Convert.ToInt32(form["Price"]);
                int subTypeId = Convert.ToInt32(form["SubscriptionType"]);
                var service = new AdminService();
                service.AddPriceMatrix(new PriceMatrixDto { Price = price, SubscriptionTypeId = subTypeId });
                return RedirectToAction("Index", "PriceMatrix");
            }

            [HttpGet]
            public ActionResult UpdatePriceMatrix(int id)
            {
                var service = new AdminService();
                var res = service.GetPriceMatrixById(id);
                var brc = service.GetSubscriptionType();
                SelectList brlist = new SelectList(brc, "Id", "SubscriptionTypeTitle", res.SubscriptionTypeId);
                ViewBag.SubscriptionType = brlist;
                return View(res);
            }

            [HttpPost]
            public ActionResult UpdatePriceMatrix(YearDto year, FormCollection form)
            {
                var service = new AdminService();
                year.BranchId = Convert.ToInt32(form["Branch"]);
                service.UpdateYear(year);
                return RedirectToAction("Index", "PriceMatrix");
            }

            public ActionResult DeletePriceMatrix(int id)
            {
                var service = new AdminService();
                service.DeletePriceMatrix(id);
                return RedirectToAction("Index", "PriceMatrix");
            }
        }
	
}