﻿using Education.Admin.Filters;
using Education.Dtos;
using Education.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Education.Admin.Controllers
{
    [AuthActionFilter]
    public class ProfessorDetailController : Controller
    {
        //
        // GET: /ProfessorDetail/
       
            //
            // GET: /ProfessorDetail/
            public ActionResult Index()
            {
                var service = new AdminService();
                var res = service.GetProfessor();
                return View(res);//Pass res
            }

            [HttpGet]
            public ActionResult AddProfessorDetail()
            {
                return View();
            }

            [HttpPost]
            public ActionResult AddProfessorDetail(FormCollection form)
            {
                var service = new AdminService();
                var name = form["ProfessorName"].ToString();
                var profile = form["Profile"].ToString();
                var image = form["ProfessorImage"].ToString();
                service.AddProfessor(new ProfessorDto { ProfessorName = name, Profile = profile, ProfessorImg = image });
                return RedirectToAction("Index", "ProfessorDetail");
            }

            [HttpGet]
            public ActionResult UpdateProfessorDetail(int id)
            {
                var service = new AdminService();
                var res = service.GetProfessorById(id);
                return View(res);
            }

            [HttpPost]
            public ActionResult UpdateProfessorDetail(ProfessorDto prof, FormCollection form)
            {
                var service = new AdminService();
                prof.ProfessorName = form["ProfessorName"].ToString();
                prof.Profile = form["Profile"].ToString();
                prof.ProfessorImg = form["ProfessorImage"].ToString();
                service.UpdateProfessor(prof);
                return RedirectToAction("Index", "ProfessorDetail");
            }

            public ActionResult DeleteProfessorDetail(int id)
            {
                var service = new AdminService();
                service.DeleteProfessor(id);
                return RedirectToAction("Index", "ProfessorDetail");
            }
        
	}
}