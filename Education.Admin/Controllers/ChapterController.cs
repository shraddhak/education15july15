﻿using Education.Admin.Filters;
using Education.Dtos;
using Education.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Education.Admin.Controllers
{
    public class ChapterController : Controller
    {
        //
        // GET: /Chapter/
        [AuthActionFilter]
        public ActionResult Index()
        {
            var service = new AdminService();
            var res = service.GetChapter();
            return View(res);
        }

        [HttpGet]
        public ActionResult AddChapter()
        {
            var service = new AdminService();
            var brc = service.GetSubject();
            SelectList brlist = new SelectList(brc, "Id", "SubjectTitle");
            ViewBag.Subject = brlist;
            return View();
        }

        [HttpPost]
        public ActionResult AddChapter(FormCollection form)
        {
            var chapter = form["ChapterTitle"].ToString();
            int subjectId = Convert.ToInt32(form["Subject"]);
            var service = new AdminService();
            service.AddChapter(new ChapterDto { Title = chapter, SubjectId = subjectId });
            return RedirectToAction("Index", "Chapter");
        }

        [HttpGet]
        public ActionResult UpdateChapter(int id)
        {
            var service = new AdminService();
            var res = service.GetChapterById(id);
            var brc = service.GetSubject();
            SelectList brlist = new SelectList(brc, "Id", "SubjectTitle", res.SubjectId);
            ViewBag.Subject = brlist;
            return View(res);
        }

        [HttpPost]
        public ActionResult UpdateChapter(ChapterDto chapter, FormCollection form)
        {
            var service = new AdminService();
            chapter.SubjectId = Convert.ToInt32(form["Subject"]);
            service.UpdateChapter(chapter);
            return RedirectToAction("Index", "Chapter");
        }

        public ActionResult DeleteChapter(int id)
        {
            var service = new AdminService();
            service.DeleteChapter(id);
            return RedirectToAction("Index", "Chapter");
        }
    }
}