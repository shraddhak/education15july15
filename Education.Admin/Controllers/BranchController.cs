﻿using Education.Admin.Filters;
using Education.Dtos;
using Education.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Education.Admin.Controllers
{
    [AuthActionFilter]
    public class BranchController : Controller
    {
        //
        // GET: /Branch/
        [HttpGet]
        public ActionResult Index()
        {
            var service = new UserService();
            var res = service.GetBranch();
            return View(res);
        }

        public ActionResult AddBranch()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddBranch(FormCollection form)
        {
            var service = new AdminService();
            service.AddBranch(form["BranchTitle"].ToString());
            return RedirectToAction("Index", "Branch");
        }

        [HttpGet]
        public ActionResult UpdateBranch(int id)
        {
            var service = new AdminService();
            var res = service.GetBranchById(id);
            return View(res);
        }

        [HttpPost]
        public ActionResult UpdateBranch(BranchDto branch)
        {
            var service = new AdminService();
            service.UpdateBranch(branch);
            return RedirectToAction("Index", "Branch");
        }

        public ActionResult DeleteBranch(int id)
        {
            var service = new AdminService();
            service.DeleteBranch(id);
            return RedirectToAction("Index", "Branch");
        }
    }
}