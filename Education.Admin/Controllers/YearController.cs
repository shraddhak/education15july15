﻿using Education.Admin.Filters;
using Education.Dtos;
using Education.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Education.Admin.Controllers
{
    [AuthActionFilter]
    public class YearController : Controller
    {
        //
        // GET: /Year/
       
            //
            // GET: /Year/
            public ActionResult Index()
            {
                var service = new AdminService();
                var res = service.GetYear();
                return View(res);
            }

            [HttpGet]
            public ActionResult AddYear()
            {
                var service = new UserService();
                var brc = service.GetBranch();
                SelectList brlist = new SelectList(brc, "Id", "BrachTitle");
                ViewBag.Branch = brlist;
                return View();
            }

            [HttpPost]
            public ActionResult AddYear(FormCollection form)
            {
                var year = form["YearTitle"].ToString();
                int branchId = Convert.ToInt32(form["Branch"]);
                var service = new AdminService();
                service.AddYear(new YearDto { YearTitle = year, BranchId = branchId });
                return RedirectToAction("Index", "Year");
            }

            [HttpGet]
            public ActionResult UpdateYear(int id)
            {
                var service = new AdminService();
                var res = service.GetYearById(id);
                var userSer = new UserService();
                var brc = userSer.GetBranch();
                SelectList brlist = new SelectList(brc, "Id", "BrachTitle", res.BranchId);
                ViewBag.Branch = brlist;
                return View(res);
            }

            [HttpPost]
            public ActionResult UpdateYear(YearDto year, FormCollection form)
            {
                var service = new AdminService();
                year.BranchId = Convert.ToInt32(form["Branch"]);
                service.UpdateYear(year);
                return RedirectToAction("Index", "Year");
            }

            public ActionResult DeleteYear(int id)
            {
                var service = new AdminService();
                service.DeleteYear(id);
                return RedirectToAction("Index", "Year");
            }
       
	}
}