﻿using Education.Admin.Filters;
using Education.Dtos;
using Education.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Education.Admin.Controllers
{
    [AuthActionFilter]
    public class SubscriptionDetailController : Controller
    {
        //
        // GET: /SubscriptionDetail/
      
            //
            // GET: /SubscriptionDetail/
            public ActionResult Index()
            {
                //var service = new AdminService();
                //var res = service.GetSubscriptionDetail();
                return View();
            }

            [HttpGet]
            public ActionResult AddSubscriptionDetail()
            {
                var service = new AdminService();
                var brc = service.GetSubscriptionType();
                SelectList brlist = new SelectList(brc, "Id", "SubscriptionTypeTitle");
                ViewBag.SubscriptionType = brlist;

                var user = service.GetUserSubscription();
                SelectList userlist = new SelectList(user, "Id", "Title");
                ViewBag.UserSubscription = userlist;

                var content = service.GetContentDetail();
                SelectList contentlist = new SelectList(content, "Id", "ContentId");
                ViewBag.ContentDetails = contentlist;
                return View();
            }

            [HttpPost]
            public ActionResult AddSubscriptionDetail(FormCollection form)
            {
                var service = new AdminService();
                int userid = Convert.ToInt16(form["UserId"]);
                int SubType = Convert.ToInt32(form["SubscriptionType"]);
                int UserSub = Convert.ToInt32(form["UserSubscription"]);
                int ContentDet = Convert.ToInt32(form["ContentDetails"]);
                string Start = form["StartTime"].ToString();
                string End = form["EndTime"].ToString();
                service.AddSubscriptionDetail(new SubscriptionDetailDto
                {
                    ContentId = ContentDet,
                    SubscriptionTypeId = SubType,
                    UserSubscriptionId = UserSub,
                    UserId = userid,
                    StartTime = Start,
                    EndTime = End
                });
                return RedirectToAction("Index", "SubscriptionDetail");
            }

            [HttpGet]
            public ActionResult UpdateSubscriptionDetail(int id)
            {
                var service = new AdminService();
                var res = service.GetSubscriptionDetailById(id);
                var brc = service.GetSubscriptionType();
                SelectList brlist = new SelectList(brc, "Id", "SubscriptionTypeTitle");
                ViewBag.SubscriptionType = brlist;

                var user = service.GetUserSubscription();
                SelectList userlist = new SelectList(user, "Id", "Title");
                ViewBag.UserSubscription = userlist;

                var content = service.GetContentDetail();
                SelectList contentlist = new SelectList(content, "Id", "ContentId");
                ViewBag.ContentDetails = contentlist;
                return View();
            }

            [HttpPost]
            public ActionResult UpdateSubscriptionDetail(SubscriptionDetailDto subDetail, FormCollection form)
            {
                var service = new AdminService();
                subDetail.UserId = Convert.ToInt16(form["UserId"]);
                subDetail.SubscriptionTypeId = Convert.ToInt32(form["SubscriptionType"]);
                subDetail.UserSubscriptionId = Convert.ToInt32(form["UserSubscription"]);
                subDetail.ContentId = Convert.ToInt32(form["ContentDetails"]);
                subDetail.StartTime = form["StartTime"].ToString();
                subDetail.EndTime = form["EndTime"].ToString();
                service.UpdateSubscriptionDetail(subDetail);
                return RedirectToAction("Index", "SubscriptionDetail");
            }

            public ActionResult DeleteSubscriptionDetail(int id)
            {
                var service = new AdminService();
                service.DeleteSubscriptionDetail(id);
                return RedirectToAction("Index", "SubscriptionDetail");
            }
        
	}
}