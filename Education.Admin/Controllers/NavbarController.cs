﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using Education.Admin.Domain;
using Education.Admin.Models;

namespace Education.Admin.Controllers
{
    public class NavbarController : Controller
    {
        //
        // GET: /Navbar/
        public ActionResult Navbar(string controller, string action)
        {
            var data = new Data();

            var navbar = data.itemsPerUser(controller, action, User.Identity.Name);
           

            return PartialView("_navbar", navbar);
            //return View();
        }
	}
}