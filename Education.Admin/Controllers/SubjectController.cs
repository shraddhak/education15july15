﻿using Education.Admin.Filters;
using Education.Dtos;
using Education.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Education.Admin.Controllers
{
    [AuthActionFilter]
    public class SubjectController : Controller
    {
        //
        // GET: /Subject/
       
            //
            // GET: /Subject/
            public ActionResult Index()
            {
                var service = new AdminService();
                var res = service.GetSubject();
                return View(res);
            }

            [HttpGet]
            public ActionResult AddSubject()
            {
                var service = new UserService();
                var brc = service.GetSemester();
                SelectList brlist = new SelectList(brc, "Id", "SemesterTitle");
                ViewBag.Semester = brlist;
                return View();
            }

            [HttpPost]
            public ActionResult AddSubject(FormCollection form)
            {
                var year = form["SubjectTitle"].ToString();
                int semId = Convert.ToInt32(form["Semester"]);
                var service = new AdminService();
                service.AddSubject(new SubjectDto { SubjectTitle = year, SemesterId = semId });
                return RedirectToAction("Index", "Subject");
            }

            [HttpGet]
            public ActionResult UpdateSubject(int id)
            {
                var service = new AdminService();
                var res = service.GetSubjectById(id);
                var userSer = new UserService();
                var brc = userSer.GetSemester();
                SelectList brlist = new SelectList(brc, "Id", "SemesterTitle", res.SemesterId);
                ViewBag.Semester = brlist;
                return View(res);
            }

            [HttpPost]
            public ActionResult UpdateSubject(SubjectDto subject, FormCollection form)
            {
                var service = new AdminService();
                subject.SemesterId = Convert.ToInt32(form["Semester"]);
                service.UpdateSubject(subject);
                return RedirectToAction("Index", "Subject");
            }

            public ActionResult DeleteSubject(int id)
            {
                var service = new AdminService();
                service.DeleteSubject(id);
                return RedirectToAction("Index", "Subject");
            }
       
	}
}