﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Education.Models
 {
    public class ProfessorProfileViewModel
    {
        public string ProfessorName { get; set; }
        public string Profile { get; set; }
        public string ProfessorImg { get; set; }
        public string CreateDate { get; set; }
        public string UpdateDate { get; set; }
        public Boolean IsRemoved { get; set; }
    }
}