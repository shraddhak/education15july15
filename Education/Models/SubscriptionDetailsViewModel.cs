﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Education.Master;
using Education.DataAccess;


namespace Education.Models
  {
    public class SubscriptionDetailsViewModel : IValidatableObject
    {        
        public int UserId { get; set; }
        public User User { get; set; }

        public int Subscriptions { get; set; }
        public int Branch { get; set; }
        public int Year { get; set; }
        public string[] Semester { get; set; }
        public string[] Subject1 { get; set; }
        public string[] Subject { get; set; }
        public string[] Chapter { get; set; }
        public bool IsvalidateFlag { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            string[] subid;
            string[] chapids = Chapter;
            if (Subscriptions == 2)
            {
                subid = Subject1;
            }
            else
            {
                subid = Subject;
            }
            string message = "";
            message = CheckIfExist(UserId, Semester, Subscriptions, subid, chapids);
            yield return new ValidationResult(message);
            if (message != "")
            {
                IsvalidateFlag = true;
            }
            else
            {
                IsvalidateFlag = false;
            }

        }


        public string CheckIfExist(int userId, string[] semIds, int subscrid, string[] subjectIds, string[] chapterids)
        {
            using (var repo = new EfUnitOfWork())
            {

                List<string> validationmessages = new List<string>();
                bool isSemesterExist = false;
                bool isSubjectExist = false;
                bool isChapterExist = false;
                string str = "";
                var ids = repo.Temp_UserSubscriptionRepository.FindMany(s => s.UserId == userId && s.SubscriptionTypeId == subscrid).ToList();
                foreach (var id in ids)
                {
                    if (isSemesterExist == false)
                    {
                        var subscriptionDetailidlst = repo.Temp_SubscriptionDetailsRepository.FindMany(x => x.TempUserSubscriptionId == id.Id).Select(s => s.Id).ToList();
                        var contentType = repo.Temp_SubscriptionDetailsRepository.FindMany(x => x.TempUserSubscriptionId == id.Id).Select(s => s.SubscriptionTypeId).FirstOrDefault();


                        foreach (var subscid in subscriptionDetailidlst)
                        {
                            var contentidlst = repo.Temp_ContentDetailsRepository.FindMany(x => x.TempSubscriptionDetailId == subscid).Select(s => s.ContentId).ToList();
                            switch (contentType)
                            {
                                case 1:
                                    foreach (var semid in semIds)
                                    {
                                        foreach (var conId in contentidlst)
                                        {
                                            int semisterid = Convert.ToInt32(semid);

                                            if (conId == semisterid)
                                            {
                                                isSemesterExist = true;  //semestrlst = res1.ToList();
                                                break;
                                            }
                                            else
                                            {
                                                isSemesterExist = false;
                                                // break;
                                            }
                                        }
                                        break;

                                    }


                                    break;
                                case 2:
                                    foreach (var subid in subjectIds)
                                    {
                                        foreach (var conId2 in contentidlst)
                                        {
                                            int sid = Convert.ToInt32(subid);
                                            if (conId2 == sid)
                                            {
                                                isSubjectExist = true;
                                                break;
                                            }
                                            else
                                            {
                                                isSubjectExist = false;
                                            }

                                        }
                                        break;


                                    }
                                    break;

                                case 3:

                                    foreach (var chapid in chapterids)
                                    {
                                        foreach (var conid3 in contentidlst)
                                        {
                                            int chid = Convert.ToInt32(chapid);
                                            if (conid3 == chid)
                                            {
                                                isChapterExist = true;
                                                break;
                                            }
                                            else
                                            {
                                                isChapterExist = false;
                                            }
                                        }

                                        break;
                                    }
                                    break;

                            }
                        }

                    }
                }
                if (isSemesterExist == true)
                {
                    str = "You have already subscribed for this semester ";
                   
                }
                if (isSubjectExist == true)
                {
                    str = "You have already subscribed for this  Subject ";
                   
                }
                if (isChapterExist == true)
                {
                    str = "You have already subscribed for  this Chapter ";
                  
                }

              
                return str;
               
            }
        }



    }
}