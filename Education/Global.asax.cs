﻿using SIND.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Education
{
    public class MvcApplication : System.Web.HttpApplication
    {
        ErrorTrackService ErrorService = new ErrorTrackService();
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        protected void Application_Error(object sender, EventArgs e)
        {
            try
            {
                Exception exception = Server.GetLastError();
                string message = exception.Message;
                string StackTrace = exception.StackTrace;
                ErrorService.InserErrorLog(StackTrace, message);
                MailMessage mail = new MailMessage();
                mail.To.Add("anand7289@gmail.com");
                mail.From = new MailAddress("bhaveanand02@gmail.com");
                mail.Subject = "Error Logged";
                string Body = message;
                mail.Body = Body;
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();

                smtp.Send(mail);
                Server.ClearError();
                Response.Redirect("/Error/Error");
            }
            catch
            { }
        }
    }
}
