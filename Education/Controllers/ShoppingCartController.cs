﻿using Education.Dtos;
using Education.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Education.Controllers
{
    public class ShoppingCartController : Controller
    {
        UserService _userSer = new UserService();
        //
        // GET: /ShoppingCart/
        public ActionResult ShopingCart()
                                                                                          {
            List<DashboardDto> resultCnt = new List<DashboardDto>();
            List<string> result = new List<string>();
            try
            {
                HttpCookie UserSessionid = Request.Cookies["UserCookieId"];
                if (UserSessionid != null)
                {
                    var userID = Session["LogedUserID"];
                    int id = Convert.ToInt16(userID);
                    result = _userSer.getu(id);
                    resultCnt = _userSer.GetListByTempCntId(id, UserSessionid.Value);
                }

            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
          
            ViewData["Result"] = result;


            return View(resultCnt);
        }
        [HttpGet]
        public ActionResult DeleteSubcribtion(int id, int contentId)
        {
            try
            {
                var service = new UserService();
                var userID = Session["LogedUserID"];
                int NewUsrId = Convert.ToInt16(userID);
                _userSer.DeleteSubscribtion(id, NewUsrId, contentId);
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
            
            return RedirectToAction("ShopingCart", "ShoppingCart");
            //return View("ShopingCart");
        }
	}
}