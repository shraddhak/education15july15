﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Education.Service;
using Education.Dtos;
using Education.DataAccess;

namespace Education.Controllers
{
    public class DashboardController : Controller
    {
        //
        // GET: /Dashboard/
        [HttpGet]
        public ActionResult Dashboard()
        {
            List<DashboardDto> resultCnt = new List<DashboardDto>();
            try
            {
                var DashboardSrv = new DashboardService();
                var userID = Session["LogedUserID"];
                int id = Convert.ToInt16(userID);
                //List<string> result = DashboardSrv.getu(id);
                resultCnt = DashboardSrv.GetListByTempCntId(id);
                //ViewData["Result"] = result;
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
            return View(resultCnt);
        }


        [HttpGet]
        public ActionResult DashboardNavigation(int item, string type, string Title)
        {
            var repo = new EfUnitOfWork();
            var service = new UserService();
            // var userID = Session["LogedUserID"];
            List<DashboardDto> resultCnt = null;
            try
            {
                var data = repo.SubjectRepository.FindMany(s => s.SemesterId == item).ToList();


                if (type == "sem")
                {
                    resultCnt = service.GetSubject1(item);
                    ViewBag.Type = "sem";
                    ViewBag.semTitle = Title;
                }
                else if (type == "chapter")
                {
                    resultCnt = service.GetChapter1(item);
                    ViewBag.Type = "chapter";
                    ViewBag.subTitle = Title;
                }
                else if (type == "lecture")
                {
                    resultCnt = service.GetLecture1(item);
                    ViewBag.Type = "lecture";
                    ViewBag.lectTitle = Title;
                }
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
           

            return View(resultCnt);
        }

         [HttpGet]
        public ActionResult test(string item)
        {
            ViewBag.URL = item;
            return View();

        }
	}
}