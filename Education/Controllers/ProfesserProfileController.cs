﻿using Education.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Education.Controllers

{
    public class ProfesserProfileController : Controller
    {
        //
        // GET: /ProfesserProfile/
        ProfessorProfileService professorService = new ProfessorProfileService();
        public ActionResult Profile()
        {          
          var data = professorService.GetData(); 
          return View(data);
        }

        //public ActionResult ShowProfessorDetails()
        //{
        //    return PartialView("_ProfessorprofilePartial");
        //}
	}
}