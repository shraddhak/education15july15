﻿using Education.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Education.Dtos;

namespace Education.Controllers

{
    public class SubscribeController : Controller
    {
        //
        // GET: /Subscribe/
        SubscribeService subscribeService = new SubscribeService();
        UserService _userSer = new UserService();
        [HttpGet]
        public ActionResult Subscribe()
        {
            try
            {
                var service = new UserService();
                var data = service.GetUser();
                SelectList userlist = new SelectList(data, "Id", "Name");
                ViewBag.Users = userlist;

                var sub = service.GetSubTypes();
                SelectList sublist = new SelectList(sub, "Id", "SubscriptionTypeTitle");
                ViewBag.Subscriptions = sublist;

                var brc = service.GetBranch();
                SelectList brlist = new SelectList(brc, "Id", "BrachTitle");
                ViewBag.Branch = brlist;

                var year = service.GetYear();
                SelectList yearlist = new SelectList(year, "Id", "YearTitle");
                ViewBag.Year = yearlist;

                var sem = service.GetSemester();
                SelectList semlist = new SelectList(sem, "Id", "SemesterTitle");
                ViewBag.Semester = semlist;

                var sub1 = service.GetSubject();
                SelectList sub1list = new SelectList(sub1, "Id", "SubjectTitle");
                ViewBag.Subject = sub1list;


                var chapter = service.GetChapter();
                SelectList chplist = new SelectList(chapter, "Id", "Title");
                ViewBag.Chapter = chplist;
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }          

            return View();
        }

        [HttpPost]
        public ActionResult Subscribe(SubscriptionDetailDto Objsubscription)
        {
            

            if (ModelState.IsValid)
            {
                // TO DO: Save the party in database.
                //   return View("Thanks");
            }
            //try
            //{
                var service = new UserService();
                var result = subscribeService.InsertData(Objsubscription);
                ViewBag.IsValidate = result.IsvalidateFlag;


                var data = service.GetUser();
                SelectList userlist = new SelectList(data, "Id", "Name");
                ViewBag.Users = userlist;

                var sub = service.GetSubTypes();
                SelectList sublist = new SelectList(sub, "Id", "SubscriptionTypeTitle");
                ViewBag.Subscriptions = sublist;

                var brc = service.GetBranch();
                SelectList brlist = new SelectList(brc, "Id", "BrachTitle");
                ViewBag.Branch = brlist;

                var year = service.GetYear();
                SelectList yearlist = new SelectList(year, "Id", "YearTitle");
                ViewBag.Year = yearlist;

                var sem = service.GetSemester();
                SelectList semlist = new SelectList(sem, "Id", "SemesterTitle");
                ViewBag.Semester = semlist;

                var sub1 = service.GetSubject();
                SelectList sub1list = new SelectList(sub1, "Id", "SubjectTitle");
                ViewBag.Subject = sub1list;


                var chapter = service.GetChapter();
                SelectList chplist = new SelectList(chapter, "Id", "Title");
                ViewBag.Chapter = chplist;
                

            //}
            //catch (Exception ex)
            //{
            //    //Code for exception handling
            //}
            if (Objsubscription.IsvalidateFlag == true)
            {
                return View();
            }
            else
            {
                return RedirectToAction("ShopingCart", "ShoppingCart");
            } 
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GetSubjectBySemesterId(int SemesterId)
        {
            //var service = new UserService();


            var states = _userSer.GetAllSubjectsBySemesterId(SemesterId);
            var result = (from s in states
                          select new
                          {
                              id = s.Id,
                              name = s.SubjectTitle
                          }).ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GetChapterBySubjectId(int SubjectId)
        {

            var states = _userSer.GetAllChaptersBySubjectId(SubjectId);
            var result = (from s in states
                          select new
                          {
                              id = s.Id,
                              name = s.Title
                          }).ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }
	}
}