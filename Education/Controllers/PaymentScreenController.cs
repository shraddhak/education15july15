﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Education.Service;
using Education.Dtos;

namespace Education.Controllers

{
    public class PaymentScreenController : Controller
    {
        //
        // GET: /PaymentScreen/
        [HttpGet]
        public ActionResult PaymentScreen(string TotalPrice)
        {
            try
            {
                var PayService = new PaymentService();
                var userID = Session["LogedUserID"];
                int id = Convert.ToInt16(userID);

                List<string> result = PayService.getu(id);
                List<DashboardDto> resultCnt = PayService.GetListByTempCntId(id);
                ViewData["Result"] = result;
                ViewBag.TotalPrice = TotalPrice;
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
          
            return View();
        }

        [HttpPost]
        public ActionResult PaymentScreenRedirect(string totalamount)
        {
            try
            {
                var PayService = new PaymentService();
                HttpCookie UserSessionid = Request.Cookies["UserCookieId"];
                var userID = Session["LogedUserID"];
                int id = Convert.ToInt16(userID);
                PayService.InsertDataIntoFinalTbl(id, UserSessionid.Value, Convert.ToInt32(totalamount));
            }
            catch (Exception ex)
            {
                //Code for exception handling
            }
           

            return RedirectToAction("Dashboard", "Dashboard");

        }

	}
}