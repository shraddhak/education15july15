﻿using Education.CustomResult;
using Education.Dtos;
using Education.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Education.Controllers
{
    public class VideoController : Controller
    {
        //
        // GET: /Video/
        
        public ActionResult Index(string item, int Id)
        {
            var service = new VideoService();
            LectureDto lectDto = new LectureDto();
            lectDto  = service.GetLectureDetails(Id);           
            ViewBag.URL = item;            
            return View(lectDto);
        }

        public ActionResult Lecture()
        {
            var service = new VideoService();
            List<LectureDto> lectDto = new List<LectureDto>();
            lectDto = service.GetAllLecture();
           
            return View(lectDto);
        }
	}
}