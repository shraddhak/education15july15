namespace SIND.Demo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EducationDataAccessSindAppContext : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AspNetUsers", "Email_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "Email_ID", c => c.String());
        }
    }
}
