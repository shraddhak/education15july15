﻿$(document).ready(function () {


    $('#login-form').validate({

        rules: {
           
            Password: {
                required: true,
                minlength: 6
            },
            
            EmailId: {
                required: true,
                email: true
            },

            agree: "required"

        },
        highlight: function (element) {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        },
        success: function (element) {
            element
            .text('OK!').addClass('valid')
            .closest('.control-group').removeClass('error').addClass('success');
        }
    });


    $('#registration-form').validate({

        rules: {
            UserName: {
                required: true,
               
                message: "test"
            },

            Password: {
                required: true,
                minlength: 6
            },
            ConfirmPassword: {
                required: true,
                minlength: 6,
                equalTo: "#Password"
            },

            EmailAddress: {
                required: true,
                email: true
            },


            Address: {
               
                required: true,
            },

            PhoneNo:
                {
                    required: true
                },

            agree: "required"

        },
        highlight: function (element) {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        },
        success: function (element) {
            element
            .text('OK!').addClass('valid')
            .closest('.control-group').removeClass('error').addClass('success');
        }
    });

}); // end document.ready