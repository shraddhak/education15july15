﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Master
{
    public class Temp_SubscriptionDetails:Entity
    {
        public Temp_SubscriptionDetails()
        {

        }

        public int UserId { get; set; }
        public User User { get; set; }

        public int SubscriptionTypeId { get; set; }
        public SubscriptionType SubscriptionType { get; set; }

        public int TempUserSubscriptionId { get; set; }

        public Temp_UserSubscription Usersubscription { get; set; }

        public int ContentId { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }

        public bool Isprocess { get; set; }

        public ICollection<Temp_ContentDetails> Temp_ContentDetails { get; set; }

    }
}
