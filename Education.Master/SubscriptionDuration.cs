﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Master
{
    public class SubscriptionDuration: Entity
    {
        public SubscriptionDuration()
       {

       }

        public string Title { get; set; }

        public int NoOfDays { get; set; }

        public int SubscriptionTypeId { get; set; }

        public SubscriptionType SubscriptionType { get; set; }
    }
}
