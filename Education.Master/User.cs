﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Master
{
    public class User: Entity
    {
        public User()
        {

        }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public string MobileNo { get; set; }

        public string Password { get; set; }

        public ICollection<UserSubcription> UserSubscription { get; set; }
        public ICollection<Temp_SubscriptionDetails> Temp_SubscriptionDetails { get; set; }

    }
}
