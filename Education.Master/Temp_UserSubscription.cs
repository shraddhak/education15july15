﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Master
{
    public class Temp_UserSubscription: Entity
    {
        public Temp_UserSubscription()
        {

         }

        public int UserId { get; set; }

        public User User { get; set; }

        public int SubscriptionTypeId { get; set; }

        public SubscriptionType SubscriptionType { get; set; }

        public string Title { get; set; }

        public string Sessionid { get; set; }

        public bool Isprocess { get; set; }

        public ICollection<Temp_SubscriptionDetails> Temp_SubscriptionDeatils { get; set; }
    }
}
