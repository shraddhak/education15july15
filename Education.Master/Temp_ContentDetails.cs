﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Master
{
    public class Temp_ContentDetails: Entity
    {
        public Temp_ContentDetails()
        {

        }

        public int ContentId { get; set; }

        public int TempSubscriptionDetailId { get; set; }

        public Temp_SubscriptionDetails Temp_SubscriptionDetails { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public bool Isprocess { get; set; }
       
    }
}
