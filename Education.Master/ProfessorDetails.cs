﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Master
{
    public class ProfessorDetails: Entity
    {
        public ProfessorDetails()
       {

       }
        public string ProfessorName { get; set; }

        public string Profile { get; set; }

        public string ProfessorImg { get; set; }

        public string DetailsDescription { get; set; }

        public string Biodata { get; set; }

        public ICollection<Lecture> Lecture { get; set; }
    }
}
