﻿using Education.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Master
{
    public class CustomerFeedback :Entity
    {
        public CustomerFeedback()
        {

        }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Subject { get; set; }
        public string SubjectRating { get; set; }
        public string Professor { get; set; }
        public string ProfessorRating { get; set; }
        public string FeedBack { get; set; }
    }
}
