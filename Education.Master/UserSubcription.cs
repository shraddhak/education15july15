﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Master
{
    public class UserSubcription: Entity
    {

        public UserSubcription()
        {

        }

        public int UserId { get; set; }
        public User User { get; set; }

        public int SubscriptionTypeId { get; set; }
        public SubscriptionType SubscriptionType { get; set; }

        public string Title { get; set; }
       

        public ICollection<SubscriptionDetails> SubscriptionDetails;
    }
}
