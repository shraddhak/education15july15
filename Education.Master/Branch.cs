﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Master
{
    public class Branch: Entity
    {
        public Branch()
        {

        }

        public string BrachTitle { get; set; }

        public ICollection<Year> Year { get; set; }

    }
}
