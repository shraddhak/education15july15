﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Master
{
    public class SubscriptionType: Entity
    {
        public SubscriptionType()
        {

        }

        public string SubscriptionTypeTitle { get; set; }

        public ICollection<UserSubcription> UserSubscription { get; set; }

        public ICollection<SubscriptionDetails> SubscriptionDetails { get; set; }

        public ICollection<Temp_UserSubscription> Temp_UserSubscription { get; set; }

        public ICollection<Temp_SubscriptionDetails> Temp_SubscriptionDetails { get; set; }

        public ICollection<PriceMatrix> PriceMatrix { get; set; }

        public ICollection<SubscriptionDuration> SubscriptionDuration { get; set; }
    }
}
