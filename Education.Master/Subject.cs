﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Master
{
   public class Subject:Entity
    {
       public Subject()
       {

       }
       public string SubjectTitle { get; set; }

       public int SemesterId { get; set; }

       public Semester Semester { get; set; }

    }
}
