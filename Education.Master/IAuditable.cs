﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Master
{
    public interface IAuditable
    {
        DateTime CreateDate { get; set; }
        DateTime UpdateDate { get; set; }
    }
}
