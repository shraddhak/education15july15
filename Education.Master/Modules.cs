﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Master
{
   public class Modules : Entity
    {
       public int Id { get; set; }
       public string ModuleName { get; set; }
       ICollection<ErrorLog> ErrorLog { get; set;}
    }
}
