﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Master
{
    public class Lecture: Entity
    {

        public Lecture()
        {

        }

        public string LectureTitle { get; set; }

        public int ChapterId { get; set; }
        public Chapter Chapter { get; set; }

       
        public string VideoURL { get; set; }

        public int ProfessorId { get; set; }

        public string LectureContent { get; set; }

        public ProfessorDetails ProfessorDetails { get; set; }

    }
}
