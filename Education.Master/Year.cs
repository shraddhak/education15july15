﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Master
{
    public class Year: Entity
    {
        public Year()
        {

        }

        public string YearTitle { get; set; }

        public int BranchId { get; set; }

        public Branch Branch { get; set; }

        public ICollection<Semester> Semester { get; set; }
    }
}
