﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Master
{
   public class Semester: Entity
    {
       public Semester()
       {

       }

       public string SemesterTitle { get; set; }

       public int YearId { get; set; }

       public Year Year { get; set; }

       public ICollection<Subject> Subject { get; set; }
    }
}
