﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Master
{
   public class Chapter:Entity
    {
       public Subject Subject { get; set; }
     
       public int SubjectId { get; set; }

       public string Title { get; set; }

       public ICollection<Lecture> Lecture { get; set; }
    }
}
