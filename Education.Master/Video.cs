﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Master
{
   public  class Video:Entity
    {
       public Video()
       {

       }
       
        public Subject Subject { get; set; }
        public int SubjectId { get; set; }

        public Chapter Chapter { get; set; }
        public int ChapterId { get; set; }

        public string VideoTitle { get; set; }
    }
}
