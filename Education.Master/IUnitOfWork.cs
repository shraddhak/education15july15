﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Master
{
    public interface IUnitOfWork:IDisposable
    {
        void Commit();
    }
}
