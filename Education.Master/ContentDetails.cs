﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Master
{
    public class ContentDetails: Entity
    {
         public ContentDetails()
        {

        }

         public int ContentId { get; set; }

         public int SubscriptionDetailId { get; set; }
         public SubscriptionDetails SubscriptionDetails { get; set; }

         public string StartDate { get; set; }

         public string EndDate { get; set; }
    }
}
