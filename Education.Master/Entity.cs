﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Education.Master
{
    public class Entity:IEntity, IAuditable
    {
        private DateTime _createDate;

        public Entity()
        {
            // for a new entity, CreateDate and UpdateDate should be the same
            CreateDate = UpdateDate = DateTime.Now;
        }

        [Key]
        public int Id { get; set; }

       public DateTime CreateDate
        {
            get { return _createDate == DateTime.MinValue ? DateTime.Now : _createDate; }
            set { _createDate = value; }
        }

        public DateTime UpdateDate { get; set; }

        public bool IsRemoved { get; set; }
    }
}
