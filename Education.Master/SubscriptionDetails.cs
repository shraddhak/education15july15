﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Master
{
    public class SubscriptionDetails: Entity
    {
        public SubscriptionDetails()
        {

        }

        public int UserId { get; set; }
        public User User { get; set; }

        public int SubscriptionTypeId { get; set; }
        public SubscriptionType SubscriptionType { get; set; }

        public int UserSubscriptionId { get; set; }
        public UserSubcription Usersubscription { get; set; }

        public int ContentId { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }

        public ICollection<ContentDetails> ContentDetails { get; set; }
    }
}
