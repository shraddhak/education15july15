﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Master
{
   public  class Orders : Entity
    {
       public Orders()
       {

       }

       public int OrderId { get; set; }
       public DateTime OrderDate { get; set; }
       public int Amount { get; set; }
       public int UserId { get; set; }
       public string  PaymentType {get; set;}
       public string  Biilling_Addresss  {get; set;}
       public  string Bililing_City  {get; set;}
       public int Billing_Pin  {get; set;}
       public string Billing_State { get; set; }

    }
}
