﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Education.Master
{
    public class UserSession: Entity
    {
        public UserSession()
        {

        }
        
        public string UserSessionID { get; set; }
        public int UserId { get; set; }
        public int OTP { get; set; }
    }
}
